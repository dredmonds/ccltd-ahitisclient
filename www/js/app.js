var jobMap;
var map;
function initJobMap(x,y,deviceType) {
  var marker;
  var lat = x;
  var long = y;
  var myLatlng = {lat: lat, lng: long};

    jobMap = new google.maps.Map(document.getElementById('map'), {
      zoom: 16,
      center: myLatlng
    });

    marker = new google.maps.Marker({
      position: myLatlng,
      map: jobMap,
      title: 'Your working location',
      icon: 'img/Location32.png'
    });
    if(deviceType === "iPad"){
      document.getElementById('map').style.height = screen.height * 0.6 + "px";
    }else if(deviceType === "android"){
      document.getElementById('map').style.height = screen.height * 0.3 + "px";
    }else{
      document.getElementById('map').style.height = screen.height * 0.5 + "px";
    }
    google.maps.event.trigger(jobMap, 'resize');
    jobMap.panTo(myLatlng);
}


function initMap(x,y) {
  var marker;
  var lat = x;
  var long = y;
  var myLatlng = {lat: lat, lng: long};

    map = new google.maps.Map(document.getElementById('map'), {
      zoom: 16,
      center: myLatlng
    });

    map.addListener('click', function(event) {
      if(marker!=null){
        marker.setMap(null);
      }
      marker = new google.maps.Marker({
        position: event.latLng,
        map: map,
        title: 'Your preferred working location',
        icon: 'img/Location32.png'
      });
      var scope = angular.element(document.getElementById("searchPanel")).scope();
          scope.$apply(function() {
            scope.searchCriteria.location.lat = event.latLng.lat();
            scope.searchCriteria.location.long = event.latLng.lng();
          });
    });
    document.getElementById('map').style.height = screen.height * 0.6 + "px";
    google.maps.event.trigger(map, 'resize');
    map.panTo(myLatlng);
}


//Custom validation function to check if two input box identical
var compareTo = function() {
    return {
        require: "ngModel",
        scope: {
            otherModelValue: "=compareTo"
        },
        link: function(scope, element, attributes, ngModel) {

            ngModel.$validators.compareTo = function(modelValue) {
                return modelValue == scope.otherModelValue;
            };

            scope.$watch("otherModelValue", function() {
                ngModel.$validate();
            });
        }
    };
};

var app = angular.module('app', ['ionic','ngCordova','angular-md5', 'angular-sha1'])

  //Store the server address into Constant.
  .constant('baseURL', 'http://hub.ahitis.com:3000')
  .constant('localURL', 'http://192.168.1.126:3000')
  .constant('loginURL', 'https://hub.ahitis.com:3333')
  .constant('localLoginURL', 'https://192.168.1.126:3333')
  //.constant('localURL', 'http://localhost:3000')

  .run(function($state, $rootScope, $ionicLoading, $ionicPlatform, $ionicPopup, $cordovaDialogs, $cordovaMedia, $cordovaToast, $http, $window, $location, baseURL, localURL, loginURL, localLoginURL){

      //start up coding (required <div ng-init="load()"> in html)
      $rootScope.regLoad = function(){
        console.log("App start");
        //Store the server address into localStorage, this make one place to modify the server address in the future.
        var localhost = false;
        if(localhost){
          window.localStorage.setItem("baseURL", localURL);
          window.localStorage.setItem("loginURL", localLoginURL);
        }else{
          window.localStorage.setItem("baseURL", baseURL);
          window.localStorage.setItem("loginURL", loginURL);
        }

        var httpURL = window.localStorage.getItem("baseURL");
        var httpsURL = window.localStorage.getItem("loginURL");
        $rootScope.baseURL = httpURL;
        $rootScope.loginURL = httpsURL;

        //If this is not mobile device, force to login screen.
        if(!ionic.Platform.isIOS() && !ionic.Platform.isAndroid() && !ionic.Platform.isIPad()){
          $state.go('login.loginForm');
          return;
        }

        //Try to get the accessToken and usertype
        var accessToken = window.localStorage.getItem("accessToken");
        var userType = window.localStorage.getItem("userType");

        //If no accessToken or no userType, force login.
        if(!accessToken || !userType){
          $state.go('login.loginForm');
          return;
        }
        else{
          //Prepare the object to send to server. (The stored accessToken, deviceUUID)
          var authObj = {
            accessToken: accessToken,
            deviceUUID: ionic.Platform.device().uuid || ""
          };
          console.log("Connecting to " + $rootScope.loginURL);
          $rootScope.loading = $ionicLoading.show({
            template: 'Loading...',
          });
          $http.post(loginURL + "/auth", authObj, {headers: {'Content-Type': 'application/json'}})
          .then(function(data){

            $http.get($rootScope.baseURL + '/skill/name?accessToken=' + accessToken, {headers: {'Content-Type': 'application/json'}})
            .then(function(data, status, headers, config){
              $rootScope.loading.hide();
              //success
              $rootScope.skills = [];
              for(var idx in data.data.skills){
                var skillObj = {};
        			  skillObj.skillid = data.data.skills[idx].skillid;
                skillObj.name = data.data.skills[idx].name;
                skillObj.icon = data.data.skills[idx].icon;
                $rootScope.skills.push(skillObj);
              }

              //Success
              if(userType === "Worker"){ //if, the user returned as worker, go to worker menu.
                $state.go('worker.main');
              }else{ //if, the user returned as user, go to user menu.
                $state.go('user.main');
              }
            },function(data, status, headers, config){
              $rootScope.loading.hide();
              //401 invalid access token, back to login
              if(data.status===401){
                $ionicPopup.alert({
                  title: "Session Timed out",
                  template: "Going back to login screen"
                }).then(function(res){
                  window.localStorage.removeItem("accessToken");
                  $state.go('login.loginForm');
                });
              }else{
                //fail
                $ionicPopup.alert({
                  title: "Error:",
                  template: data.data.result
                })
              }
            });

            $http.get($rootScope.baseURL + '/price?accessToken=' + accessToken, {headers: {'Content-Type': 'application/json'}})
            .then(function(data,status,headers,config){
              $rootScope.loading.hide();
              //Success
              $rootScope.prices = [];
              for(var idx in data.data.prices){
                var priceObj = {};
                priceObj.skillID = data.data.prices[idx].skillID;
                priceObj.price = data.data.prices[idx].price;
                $rootScope.prices.push(priceObj);
              }
            }, function(data,status,headers,config){
              $rootScope.loading.hide();
              //401 invalid access token, back to login
              if(data.status===401){
                $ionicPopup.alert({
                  title: "Session Timed out",
                  template: "Going back to login screen"
                }).then(function(res){
                  window.localStorage.removeItem("accessToken");
                  $state.go('login.loginForm');
                });
              }else{
                //fail
                $ionicPopup.alert({
                  title: "Error:",
                  template: data.data.result
                })
              }
            });

          },function(data, status, headers, config){
            $rootScope.loading.hide();
            //error
            var result = data.data;
            var title = "";
            //error code 419, authentication required. force login.
            if(data.status === 419){
              $state.go('login.loginForm');
              return;
            }else if(data.status === 401){ //error code 401, unauthorized. force login
                $ionicPopup.alert({
                  title: "Session Timed out",
                  template: "Going back to login screen"
                }).then(function(res){
                  window.localStorage.removeItem("accessToken");
                  $state.go('login.loginForm');
                });
            }
            else{
              title = "Error";
              if(result === null){
                  result = {};
                  result.result = "Connection timeout. Please check your connection.";
                  //result.result = JSON.stringify({data: data, status: status, headers: headers, config: config});
              }
              var alertPopup = $ionicPopup.alert({
                title: title,
                template: result.result
              });
              alertPopup.then(function(res){
                console.log("Back to beginning.");
                $rootScope.regLoad();
              });
            }
          });
        }
      };

        $ionicPlatform.ready(function() {
          if(window.cordova && window.cordova.plugins.Keyboard) {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);

            // Don't remove this line unless you know what you are doing. It stops the viewport
            // from snapping when text inputs are focused. Ionic handles this internally for
            // a much nicer keyboard experience.
            cordova.plugins.Keyboard.disableScroll(true);
          }
          if(window.StatusBar) {
            StatusBar.styleDefault();
          }
        });
  });
app.directive('compareTo', compareTo);
