/*****************************************************************
* Module Name: depositCard
* Description: upload deposit card photo controller
* Author: Brian Lai
* Date Modified: 2016.06.06
* Related files: depositCard.html
*****************************************************************/

//Add a new directive to use in html. it will pass to compareTo function above.
app.controller('depositCardCtrl', function($scope,$rootScope, $state, $stateParams, $http, $ionicPopup,$ionicLoading, $cordovaInAppBrowser) {

  $scope.openWindow = function(){
    var options = {
      location: 'yes',
      toolbar: 'yes',
    };

	var targetURL = "https://www.gov.uk/government/organisations/disclosure-and-barring-service/about";
	var URL = encodeURI(targetURL);
	$cordovaInAppBrowser.open(URL, '_system', options)
	.then(function(event){
		console.log("DBS Information window open.");
	},function(event){
		console.log("Error opening for DBS Information page.");
	});
  }

  //Prepare the objects
  $scope.account = {
    userType: "Worker"
  };
  $scope.baseURL = {};
  $scope.years = [];
  var pictureSource;   // picture source
	var destinationType; // sets the format of returned value

  $scope.load = function(){
    console.log("I'm in deposit Card details");
    //$scope.title = "Deposit Card Details";

    for(var i = 1950;i<=2016;i++){
      $scope.years.push(i.toString());
    }
/*
    if (!navigator.camera)
    {
    // error handling
    return;
    }
    //pictureSource=navigator.camera.PictureSourceType.PHOTOLIBRARY;
    pictureSource=navigator.camera.PictureSourceType.CAMERA;
    destinationType=navigator.camera.DestinationType.FILE_URI;
*/
  };

/*
  // take picture
$scope.takePicture = function() {
  //console.log("got camera button click");
  var options =   {
    quality: 90,
    destinationType: destinationType,
    sourceType: pictureSource,
    encodingType: 0
    };
  if (!navigator.camera)
    {
    // error handling
    return;
    }
  navigator.camera.getPicture(
    function (imageURI) {
      //console.log("got camera success ", imageURI);
      $scope.baseURL = window.localStorage.getItem('baseURL');
      var accessToken = window.localStorage.getItem('accessToken');
      $scope.mypicture = imageURI;
      $scope.uploadurl = $scope.baseURL + "/uploads/depositCard";
      if (!$scope.uploadurl)
  			{
  			// error handling no upload url
  			return;
  			}
  		if (!$scope.mypicture)
  			{
  			// error handling no picture given
  			return;
  			}
  		var options = new FileUploadOptions();
  		options.fileKey="depositCard";
  		options.fileName=$scope.mypicture.substr($scope.mypicture.lastIndexOf('/')+1);
  		options.mimeType="image/jpeg";
      options.chunkedMode = false;
      options.headers = {
        Connection: "close"
      };
      options.params = {};
      console.log($stateParams.loginID);
      options.params = {
        "loginID": $stateParams.loginID,
        "fileName": options.fileName
      };

  		//var params = {};
  		//params.other = obj.text; // some other POST fields
  		//options.params = params;

  		//console.log("new imp: prepare upload now");
  		var ft = new FileTransfer();
      $rootScope.loading = $ionicLoading.show({
        template: 'Uploading...' + $stateParams.loginID,
      });
  		ft.upload($scope.mypicture, encodeURI($scope.uploadurl), uploadSuccess, uploadError, options, true);
  		function uploadSuccess(r) {
  			$scope.account.depositCardPic = options.fileName;
        $rootScope.loading.hide();
        console.log("Upload success");
  		}

  		function uploadError(error) {
  			console.log("upload error source " + error.source);
  			console.log("upload error target " + error.target);
        console.log(JSON.stringify({error}));
        $rootScope.loading.hide();
  		}
    },
    function (err) {
      //console.log("got camera error ", err);
      // error handling camera plugin
      },
    options);
  };
*/
  //upload button
  $scope.upload = function(account){
    //server URL
      $scope.baseURL = window.localStorage.getItem('baseURL');

      $scope.accessToken = window.localStorage.getItem('accessToken');
      //prepare the register object to send to server.
      var regObj = {
    	DBSref: account.DBSref,
        cardName: account.cardName,
        cardAddress: account.cardAddress,
        cardCity: account.cardCity,
        cardPostCode: account.cardPostCode,
        cardDay: account.cardBirthDay,
        cardMonth: account.cardBirthMonth,
        cardYear: account.cardBirthYear,
        cardBankName: account.cardBankName,
        cardAccountNumber: account.cardAccountNumber,
        cardSortCode: account.cardSortCode,
        cardIBAN: account.cardIBAN,
        cardSwiftCode: account.cardSwiftCode,
        accessToken: $scope.accessToken
      };
      //register
      $rootScope.loading = $ionicLoading.show({
        template: 'Loading...',
      });

      $http.post($scope.baseURL + '/register/depositCard', regObj, {headers: {'Content-Type': 'application/json'}})
      .then(function(data, status, headers, config){
        $rootScope.loading.hide();
        //success
        var result = data.data;
        $state.go("worker.main");
      },function(data, status, headers, config){
        $rootScope.loading.hide();
        //401 invalid access token, back to login
        if(data.status===401){
          $ionicPopup.alert({
            title: "Session Timed out",
            template: "Going back to login screen"
          }).then(function(res){
            window.localStorage.removeItem("accessToken");
            $state.go('login.loginForm');
          });
        }else{
          //Error
          $ionicPopup.alert({
            title: "Error:",
            template: data.data.result
          });
        }
      });
  };
});
