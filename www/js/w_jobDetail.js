/*****************************************************************
* Module Name: Worker's Job Detail
* Description: Worker's Job Detail controller
* Author: Brian Lai
* Date Modified: 2016.05.06
* Related files: w_jobDetail.html
*****************************************************************/
// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'

//Implement the controller profile.js

app.controller('wJobDetailCtrl', function($scope, $rootScope, $http, $interval, $ionicHistory, $state, $cordovaDialogs, $ionicPopup, $window) {
  $scope.job = {};
  $scope.action = {};
  $scope.action.acceptText = "Accept";
  $scope.action.acceptCount = 0;

	$scope.load = function(){
		$scope.baseURL = window.localStorage.getItem('baseURL');
		var accessToken = window.localStorage.getItem('accessToken');
    var deviceType = "";
    if(ionic.Platform.isIPad()){
      deviceType = "iPad";
    }else if(ionic.Platform.isAndroid()){
      deviceType = "android";
    }else{
      deviceType = "others";
    }

    //if($rootScope.job){
    //  $scope.job = $rootScope.job;
    //  $rootScope.jobID = $scope.job.jobID;
    //  initJobMap($scope.job.loc[1],$scope.job.loc[0]);
    //}else
    if($rootScope.jobID){
      var jobReqObj = {
          accessToken: accessToken,
          jobID: $rootScope.jobID
      };

      $http.post($scope.baseURL + '/job/jobDetail', jobReqObj, {headers: {'Content-Type': 'application/json'}})
      .then(function(data){

        if(data.status == 200){
          var jobObj = {};
          jobObj.jobID = data.data.job.jobID;
          jobObj.userID = data.data.job.userID;
          jobObj.workerID = data.data.job.workerID;
          jobObj.skillID = data.data.job.skillID;

          for(var skillIdx in $rootScope.skills){
            if($rootScope.skills[skillIdx].skillid === data.data.job.skillID){
              jobObj.skill = $rootScope.skills[skillIdx].name;
              jobObj.skillIcon = $rootScope.skills[skillIdx].icon;
            }
          }
          jobObj.loc = data.data.job.loc;

          jobObj.statusID = data.data.job.statusID;
          switch(data.data.job.statusID){
            case 1:
              jobObj.status = "Waiting for acceptance";
              break;
            case 2:
              jobObj.status = "In progress";
              break;
            case 3:
              jobObj.status = "Completed";
              break;
            case 4:
              jobObj.status = "In dispute";
              break;
            case 5:
              jobObj.status = "Rejected";
              break;
            default:
              break;
          }

          jobObj.paymentStatusID = data.data.job.paymentStatusID;
          switch(data.data.job.paymentStatusID){
            case 1:
              jobObj.paymentStatus = "Pending";
              break;
            case 2:
              jobObj.paymentStatus = "Completed";
              break;
            case 3:
              jobObj.paymentStatus = "Canceled";
              break;
            default:
              break;
          }

          jobObj.createdDate = data.data.job.createdDate;

          var userReqObj = {
              accessToken: accessToken,
              userID: jobObj.userID
          };

          $http.post($scope.baseURL + '/user/getUser', userReqObj, {headers: {'Content-Type': 'application/json'}})
          .then(function(data){
            console.log("getUser: " + data.status);
            if(data.status == 200){
              if(data.data.profilePict){
                jobObj.userIcon = data.data.profilePict;
              }
              if(data.data.contactNo){
                jobObj.contactNumber = data.data.contactNo;
              }

              $scope.job = jobObj;
              initJobMap($scope.job.loc[1],$scope.job.loc[0],deviceType);
              var currentDate = new Date(data.data.currentDate);
              var jobDate = new Date(jobObj.createdDate);
              var timeDiff = 30000 - (currentDate.getTime() - jobDate.getTime());
              timeDiff = Math.ceil(timeDiff / 1000);

              $scope.action.acceptCount = timeDiff;
              jobObj.createdDate = jobObj.createdDate.replace('T', ' ').replace('Z', ' ');
              console.log("acceptCount: " + timeDiff);
              console.log("stop: " + JSON.stringify(stop));
              if(!$rootScope.stop && jobObj.statusID === 1){
                $rootScope.stop = $interval(function(){
                  console.log("acceptCount: " + $scope.action.acceptCount);
                  if($scope.action.acceptCount > 0){
                    //count down
                    $scope.action.acceptText = "Accept {" + $scope.action.acceptCount + "}";
                    $scope.action.acceptCount--;
                  }else if($scope.action.acceptCount === 0){
                    //Call reject
                    $scope.jobReject();
                  }
                }, 1000,timeDiff + 1);
                console.log("intervalId: " + $rootScope.stop.$$intervalId);
              }
            }
          }, function(data, status, headers, config){
            console.log("getUser: " + data.status);
            //401 invalid access token, back to login
            if(data.status===401){
              $ionicPopup.alert({
                title: "Session Timed out",
                template: "Going back to login screen"
              }).then(function(res){
                window.localStorage.removeItem("accessToken");
                $state.go('login.loginForm');
              });
            }else{
              //fail
              $ionicPopup.alert({
                title: "Error:",
                template: data.data.result
              });
            }
          });

        }
      }, function(data, status, headers, config){
        //401 invalid access token, back to login
        if(data.status===401){
          $ionicPopup.alert({
            title: "Session Timed out",
            template: "Going back to login screen"
          }).then(function(res){
            window.localStorage.removeItem("accessToken");
            $state.go('login.loginForm');
          });
        }else{
          //fail
          $ionicPopup.alert({
            title: "Error:",
            template: data.data.result
          });
        }
      });

    }
	};

  //job accept
  $scope.jobAccept = function(){
		var accessToken = window.localStorage.getItem('accessToken');
    var jobReqObj = {
        accessToken: accessToken,
        jobID: $rootScope.jobID
    };

    $interval.cancel($rootScope.stop);
    $rootScope.stop = undefined;

    console.log("Interval shoudl be stopped");

    $http.post($scope.baseURL + '/job/jobAccept', jobReqObj, {headers: {'Content-Type': 'application/json'}})
    .then(function(data){

      if(data.status == 200){
        $cordovaDialogs.alert("Job Accepted", "Job");
        $state.reload();
      }else if(data.status == 204){
        $ionicPopup.alert({
          title: "Job Lose",
          template: "Job is accepted by another worker."
        }).then(function(res){
          $state.go("worker.main");
        });
      }
    }, function(data, status, headers, config){
      //401 invalid access token, back to login
      if(data.status===401){
        $ionicPopup.alert({
          title: "Session Timed out",
          template: "Going back to login screen"
        }).then(function(res){
          window.localStorage.removeItem("accessToken");
          $state.go('login.loginForm');
        });
      }else{
        //fail
        $ionicPopup.alert({
          title: "Error:",
          template: data.data.result
        });
      }
    });

  };

  //job reject
  $scope.jobReject = function(){
    var accessToken = window.localStorage.getItem('accessToken');
    var jobReqObj = {
        accessToken: accessToken,
        jobID: $rootScope.jobID
    };

    $interval.cancel($rootScope.stop);
    $rootScope.stop = undefined;

    console.log("firing local job reject");
    $http.post($scope.baseURL + '/onlineWorker/jobReject', jobReqObj, {headers: {'Content-Type': 'application/json'}})
    .then(function(data){

      if(data.status == 200){
        $cordovaDialogs.alert("Job Rejected", "Job");
        $state.go("worker.main");
      }
    }, function(data, status, headers, config){
      //401 invalid access token, back to login
      if(data.status===401){
        $ionicPopup.alert({
          title: "Session Timed out",
          template: "Going back to login screen"
        }).then(function(res){
          window.localStorage.removeItem("accessToken");
          $state.go('login.loginForm');
        });
      }else{
        //fail
        $ionicPopup.alert({
          title: "Error:",
          template: data.data.result
        });
      }
    });

  };

  $scope.jobDispute = function(){
    //To do
    $state.go('worker.help.disputes', {"jobID":$rootScope.jobID});
  };

});
