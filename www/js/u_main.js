/*****************************************************************
* Module Name: Main page Option's
* Description: User's main page controller
* Author: Brian Lai
* Date Modified: 2016.08.16
* Related files: u_main.html
*****************************************************************/
// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
app.controller('u_mainCtrl', function($scope, $rootScope, $state, $http, $ionicHistory, $ionicPopup) {
  //prepare objects.
  $scope.user = {};

  //page load
  $scope.load = function(){
    if(ionic.Platform.isIPad()){
      $scope.homePic = "height: 60%; width: 60%";
    }else{
      $scope.homePic = "height: 50%; width: 50%";
    }

    $scope.user.firstname = window.localStorage.getItem('firstname');
    $scope.user.lastname = window.localStorage.getItem('lastname');
  };
})
