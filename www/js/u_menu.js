/*****************************************************************
* Module Name: Menu Option's
* Description: User's sidebar menu options controller
* Author: dredmonds
* Date Modified: 2015.12.29
* Related files: menu.html
*****************************************************************/
// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
app.controller('u_menuCtrl', function($scope, $rootScope, $state, $http, $ionicHistory, $ionicPopup, md5) {
  //prepare objects.
  $scope.user = {};
  $scope.baseURL = {};

  $scope.goHome = function(){
    $ionicHistory.clearHistory();
    $state.go('user.main');
  }

  $scope.goProfile = function(){
    // .fromTemplateUrl() method
    /*
    $ionicPopover.fromTemplateUrl(resolves.profilePopOverUrl, {
      scope: $scope
    }).then(function(popover) {
      $scope.popover = popover;
    });
    */
    $state.go('user.profile');
  }

  $scope.goSearch = function(){
    $rootScope.searchCriteria = {};
    $state.go('user.search.skill');
  }

  $scope.goJob = function(){
    $state.go('user.jobList', {"scope": "current"});
  };

  $scope.goHistory = function(){
    $state.go('user.jobList', {"scope": "history"});
  };

  $scope.goHelp = function(){
    $state.go('user.help');
  }

  $scope.goEdit = function(){
	  //$state.go('user.edit');
	  //$scope.closePopover();
	  $ionicPopup.prompt({
		  title: 'VERIFY ACCOUNT',
		  template: 'Enter your secret password',
		  inputType: 'password',
		  inputPlaceholder: 'Your password',
	  }).then(function(res) {

		  if(res == ''){
			  $ionicPopup.alert({
				  title: 'Error Message',
				  template: 'Please enter your secret password.'
			  }).then(function(res){
				  $scope.goEdit();
			  });
		  }else if(res !== undefined){
			  var accessToken = window.localStorage.getItem('accessToken');
			  var usrpwdObj = {
					  accessToken: accessToken,
					  password: md5.createHash(res || '')
			  };

			  $http.post($scope.baseURL + '/usrAuthPassword', usrpwdObj, {headers: {'Content-Type': 'application/json'}})
			  .then(function(data){

				  if(data.status == 200){
					  $state.go('user.edit');
					  //$scope.closePopover();
				  }else if(data.status == 403) {
					  $ionicPopup.alert({
						  title: 'Error Message',
						  template: 'Please enter a correct password.'
					  }).then(function(response){
						  $scope.goEdit();
					  });
				  }else if(data.status===401){
              $ionicPopup.alert({
                title: "Session Timed out",
                template: "Going back to login screen"
              }).then(function(res){
                window.localStorage.removeItem("accessToken");
                $state.go('login.loginForm');
              });
          }
			  });
		  }
      });
  }

  $scope.navBack = function(){
    if($state.current.name !== "user.main"){
      $ionicHistory.goBack();
    }
  }

  //page load
  $scope.load = function(){
    //obtain the server URL
    $scope.baseURL = window.localStorage.getItem('baseURL');

    //send pushID to server
    var pushID = window.localStorage.getItem('pushID');
    var accessToken = window.localStorage.getItem('accessToken');

    if(pushID && accessToken){
      var pushObj = {
        accessToken: accessToken,
        pushID: pushID
      }

      $http.post($scope.baseURL + '/regPush', pushObj, {headers: {'Content-Type': 'application/json'}})
      .then(function(data){
        //success
      },function(data){
        //401 invalid access token, back to login
        if(data.status===401){
          $ionicPopup.alert({
            title: "Session Timed out",
            template: "Going back to login screen"
          }).then(function(res){
            window.localStorage.removeItem("accessToken");
            $state.go('login.loginForm');
          });
        }else{
          //error
          $ionicPopup.alert({
            title: "Error:",
            template: data.data.result
          });
        }
      });

      $http.get($scope.baseURL + '/user?accessToken=' + accessToken, {headers: {'Content-Type': 'application/json'}})
      .then(function(data, status, headers, config){
        if(data.data){
          var activationStatus = data.data.activationStatus;
          console.log("Activation Status: " + activationStatus);
          if(activationStatus===0){
            $state.go('login.activation');
          }
        }
      },function(data,status,headers,config){
        //401 invalid access token, back to login
        if(data.status===401){
          $ionicPopup.alert({
            title: "Session Timed out",
            template: "Going back to login screen"
          }).then(function(res){
            window.localStorage.removeItem("accessToken");
            $state.go('login.loginForm');
          });
        }else{
          //error
          $ionicPopup.alert({
            title: "Error:",
            template: data.data.result
          });
        }
      });
    }
  };
})
