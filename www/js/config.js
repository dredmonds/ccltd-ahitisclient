app.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider) {
  $urlRouterProvider.otherwise('/');
  $ionicConfigProvider.views.maxCache(0);
  $stateProvider
  .state('index',{
    url: '/',
    templateUrl: 'app.html',
    controller: 'indexCtrl'
  })

  .state('login',{
    url: '/login',
    abstract: true,
    templateUrl: 'view/login.html',
    controller: 'loginCtrl'
  })

  .state('login.loginForm',{
    url: '/loginForm',
    views: {
        'content': {
          templateUrl: 'view/loginForm.html',
          controller: 'loginFormCtrl'
        }
    }
  })

  .state('login.forgotPassword',{
    url: '/forgotPassword',
    views: {
        'content': {
          templateUrl: 'view/forgotPassword.html',
          controller: 'forgotPasswordCtrl'
        }
    }
  })

  .state('login.resetPassword',{
    url: '/resetPassword',
    views: {
        'content': {
          templateUrl: 'view/resetPassword.html',
          controller: 'resetPasswordCtrl'
        }
    }
  })

  .state('login.activation',{
    url: '/activation',
    views: {
        'content': {
          templateUrl: 'view/activation.html',
          controller: 'activationCtrl'
        }
    }
  })

  .state('reg',{
    url: '/reg',
    abstract: true,
    templateUrl: 'view/register.html',
    controller: 'regCtrl'
  })

  .state('reg.regForm',{
    url: '/regForm',
    views: {
        'content': {
            templateUrl: 'view/regForm.html',
            controller: 'regFormCtrl'
        }
    }
  })

  .state('reg.depositCard',{
    url: '/depositCard?loginID',
    views: {
      'content':{
        templateUrl: 'view/depositCard.html',
        controller: 'depositCardCtrl'
      }
    }
  })


  /* search worker $state inserted by brian 2016.02.05*/
  .state('worker',{
    url: '/worker',
    abstract: true,
    templateUrl: 'view/w_menu.html',
    controller: 'w_menuCtrl'
  })

  .state('worker.main',{
    url: '/main',
    views: {
        'content': {
            templateUrl: 'view/w_main.html',
            controller: 'w_mainCtrl'
        }
    }
  })

  .state('worker.profile',{
    url: '/profile',
    views: {
        'content': {
            templateUrl: 'view/w_profile.html',
            controller: 'wProfileCtrl'
        }
    }
  })

  .state('worker.edit',{
    url: '/profileEdit?loginID',
    views: {
        'content': {
            templateUrl: 'view/w_profile_edit.html',
            controller: 'wProfileEditCtrl'
        }
    }
  })

  .state('worker.depositcard',{
    url: '/depositCard?loginID',
    views: {
      'content':{
        templateUrl: 'view/w_depositCard_edit.html',
        controller: 'wDepositCardCtrl'
      }
    }
  })

  .state('worker.jobList',{
    url: '/jobList?scope',
    views:{
      'content': {
        templateUrl: 'view/w_jobList.html',
        controller: 'wJobListCtrl'
      }
    }
  })

  .state('worker.jobDetail',{
    url: '/jobDetail',
    views:{
      'content': {
        templateUrl: 'view/w_jobDetail.html',
        controller: 'wJobDetailCtrl'
      }
    }
  })

  .state('worker.help',{
    url: '/help',
    views: {
        'content': {
            templateUrl: 'view/w_help.html',
            controller: 'wHelpCtrl'
        }
    }
  })

  /* HR - 12/04/2016 Added next states to split help html's etc */
  .state('worker.help.disputes',{
    url: '/dispute?jobID',
    views: {
        'content': {
            templateUrl: 'templates/help_disputes.html',
            controller: 'wHelpCtrl'
        }
    }
  })

  .state('worker.help.jobList',{
    url: '/dispute?scope',
    views: {
        'content': {
            templateUrl: 'view/w_jobList.html',
            controller: 'wJobListCtrl'
        }
    }
  })

  .state('worker.help.jobtypes',{
    url: '/jobtypes',
    views: {
        'content': {
            templateUrl: 'templates/help_jobTypes.html',
            controller: 'wHelpCtrl'
        }
    }
  })

    .state('worker.jtexample',{
     url: '/jtexample',
     views: {
         'content': {
             templateUrl: 'templates/help_jtexample.html',
             controller: 'wHelpCtrl'
         }
     }
   })

  .state('worker.help.creatingajob',{
    url: '/createajob',
    views: {
        'content': {
            templateUrl: 'templates/help_creatingaJob.html',
            controller: 'wHelpCtrl'
        }
    }
  })

   .state('worker.help.qanda',{
     url: '/qanda',
     views: {
         'content': {
             templateUrl: 'templates/help_qanda.html',
             controller: 'wHelpCtrl'
         }
     }
   })

 /* HR - 12/04/2016 Added previous states to split help html's etc */

/* end worker $state */

   /* User profile $state inserted by dredmonds 2016.01.28*/
  .state('user',{
    url: '/user',
    abstract: true,
    templateUrl: 'view/u_menu.html',
    controller: 'u_menuCtrl'
  })

  .state('user.main',{
    url: '/main',
    views: {
        'content': {
            templateUrl: 'view/u_main.html',
            controller: 'u_mainCtrl'
        }
    }
  })

  .state('user.profile',{
    url: '/profile',
    views: {
        'content': {
            templateUrl: 'view/u_profile.html',
            controller: 'uProfileCtrl'
        }
    }
  })

  .state('user.edit',{
    url: '/profileEdit?loginID',
    views: {
        'content': {
            templateUrl: 'view/u_profile_edit.html',
            controller: 'uProfileEditCtrl'
        }
    }
  }) /* End User profile $state*/

 .state('user.search',{
   url: '/search',
   abstract: true,
   views: {
     'content':{
       templateUrl: 'view/search_worker.html',
       controller: 'searchWorkerCtrl'
     }
   }
 })

 .state('user.search.skill',{
   url: '/skill',
   views: {
       'searchPanel': {
         templateUrl: 'templates/search_skill.html',
         controller: 'searchWorkerCtrl'
       }
   }
 })

 .state('user.search.duration',{
   url: '/duration',
   views: {
       'searchPanel': {
         templateUrl: 'templates/search_duration.html',
         controller: 'searchWorkerCtrl'
       }
   }
 })

 .state('user.search.locationType',{
   url: '/locationType',
   views: {
       'searchPanel': {
         templateUrl: 'templates/search_locationType.html',
         controller: 'searchWorkerCtrl'
       }
   }
 })

 .state('user.search.map',{
   url: '/map',
   views: {
       'searchPanel': {
         templateUrl: 'templates/search_map.html',
         controller: 'searchWorkerCtrl'
       }
   }
 })

 .state('user.help',{
   url: '/help',
   views: {
       'content': {
           templateUrl: 'view/u_help.html',
           controller: 'uHelpCtrl'
       }
   }
 })

 .state('user.jobList',{
   url: '/jobList?scope',
   views:{
     'content': {
       templateUrl: 'view/u_jobList.html',
       controller: 'uJobListCtrl'
     }
   }
 })

 .state('user.jobDetail',{
   url: '/jobDetail',
   views:{
     'content': {
       templateUrl: 'view/u_jobDetail.html',
       controller: 'uJobDetailCtrl'
     }
   }
 })

 /* Payment profile $state inserted by dredmonds 2016.02.15*/
 .state('user.payment',{
   url: '/payment',
   abstract: true,
   views: {
     'content':{
       templateUrl: 'view/payment_menu.html',
       controller: 'payment_menuCtrl'
     }
   }
 })

 .state('user.payment.main',{
   url: '/main?jobID&skillID&countryCode&paymentStatus',
   views: {
       'content': {
           templateUrl: 'view/payment_main.html',
           controller: 'payment_Ctrl'
       }
   }
 })

 .state('user.payment.status',{
   url: '/status?jobID&txnID&amt&status',
   views: {
       'content': {
           templateUrl: 'view/payment_status.html',
           controller: 'paymentStatus_Ctrl'
       }
   }
 })


   .state('user.help.disputes',{
     url: '/dispute?jobID',
     views: {
         'content': {
             templateUrl: 'templates/help_disputes.html',
             controller: 'uHelpCtrl'
         }
     }
   })

   .state('user.help.jobList',{
     url: '/dispute?scope',
     views: {
         'content': {
             templateUrl: 'view/u_jobList.html',
             controller: 'uJobListCtrl'
         }
     }
   })

   .state('user.help.jobtypes',{
     url: '/jobtypes',
     views: {
         'content': {
             templateUrl: 'templates/help_jobTypes.html',
             controller: 'uHelpCtrl'
         }
     }
   })

   .state('user.jtexample',{
    url: '/jtexample',
    views: {
        'content': {
            templateUrl: 'templates/help_jtexample.html',
            controller: 'uHelpCtrl'
        }
    }
  })

   .state('user.help.creatingajob',{
     url: '/createajob',
     views: {
         'content': {
             templateUrl: 'templates/help_creatingaJob.html',
             controller: 'uHelpCtrl'
         }
     }
   })

   .state('user.help.qanda',{
     url: '/qanda',
     views: {
         'content': {
             templateUrl: 'templates/help_qanda.html',
             controller: 'uHelpCtrl'
         }
     }
   })

 .state('transitionPaypal',{
   url: '/transitionPaypal?orderID&buttonID&hours&amount&currency',
   templateUrl: 'view/transition_paypal.html',
   controller: 'transitionPaypal_Ctrl'
 })

})
