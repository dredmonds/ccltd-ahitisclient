/*****************************************************************
* Module Name: Worker Menu Option's
* Description: Worker's sidebar menu options controller
* Author: BrianLai
* Date Modified: 2015.01.05
* Related files: w_menu.html
*****************************************************************/
// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'

app.controller('w_menuCtrl', function($scope, $rootScope, $state, $http, $ionicHistory, $ionicPopup, $cordovaGeolocation) {
  //prepare objects.
  $scope.worker = {};
  $scope.baseURL = {};

  $scope.goHome = function(){
    $ionicHistory.clearHistory();
    $state.go('worker.main');
  }

  $scope.goJob = function(){
    $state.go('worker.jobList', {"scope":"current"});
  };

  $scope.goHistory = function(){
    $state.go('worker.jobList', {"scope":"history"});
  };

  $scope.goHelp = function(){
    $state.go('worker.help');
  }

  $scope.goProfile = function(){
    // .fromTemplateUrl() method
    /*
    $ionicPopover.fromTemplateUrl(resolves.profilePopOverUrl, {
      scope: $scope
    }).then(function(popover) {
      $scope.popover = popover;
    });
    */
    $state.go('worker.profile');
  }

  $scope.goEdit = function(){
	  //$state.go('worker.edit');
	  //$scope.closePopover();
	  $ionicPopup.prompt({
		  title: 'VERIFY ACCOUNT',
		  template: 'Enter your secret password',
		  inputType: 'password',
		  inputPlaceholder: 'Your password',
	  }).then(function(res) {

		  if(res == ''){
			  $ionicPopup.alert({
				  title: 'Error Message',
				  template: 'Please enter your secret password.'
			  }).then(function(res){
				  $scope.goEdit();
			  });
		  }else if(res !== undefined){
			  var accessToken = window.localStorage.getItem('accessToken');
			  var wkrpwdObj = {
					  accessToken: accessToken,
					  password: md5.createHash(res || '')
			  };

			  $http.post($scope.baseURL + '/wkrAuthPassword', wkrpwdObj, {headers: {'Content-Type': 'application/json'}})
			  .then(function(data){

				  if(data.status == 200){
					  $state.go('worker.edit');
					  //$scope.closePopover();
				  }else if(data.status == 403) {
					  $ionicPopup.alert({
						  title: 'Error Message',
						  template: 'Please enter a correct password.'
					  }).then(function(response){
						  $scope.goEdit();
					  });
				  }else if(data.status===401){
              $ionicPopup.alert({
                title: "Session Timed out",
                template: "Going back to login screen"
              }).then(function(res){
                window.localStorage.removeItem("accessToken");
                $state.go('login.loginForm');
              });
          }
			  });
		  }
    });
  }


/*  $scope.goHelp = function(){
    $state.go('worker.help');
    $ionicSideMenuDelegate.toggleLeft();
  }*/

  $scope.navBack = function(){
    if($state.current.name !== "worker.main"){
      $ionicHistory.goBack();
    }
  }

  //page load
  $scope.load = function(){
    //obtain the server URL
    $scope.baseURL = window.localStorage.getItem('baseURL');

    //send pushID to server
    var pushID = window.localStorage.getItem('pushID');
    var accessToken = window.localStorage.getItem('accessToken');

    if(pushID && accessToken){
      var pushObj = {
        accessToken: accessToken,
        pushID: pushID
      }

      $http.post($scope.baseURL + '/regPush', pushObj, {headers: {'Content-Type': 'application/json'}})
      .then(function(data){
        //success
      },function(data){
        //401 invalid access token, back to login
        if(data.status===401){
          $ionicPopup.alert({
            title: "Session Timed out",
            template: "Going back to login screen"
          }).then(function(res){
            window.localStorage.removeItem("accessToken");
            $state.go('login.loginForm');
          });
        }else{
          //error
          $ionicPopup.alert({
            title: "Error:",
            template: data.data.result
          });
        }
      });

    }

    $http.get($scope.baseURL + '/worker?accessToken=' + accessToken, {headers: {'Content-Type': 'application/json'}})
    .then(function(data, status, headers, config){
      if(data.data){
        var accountNumber = data.data.accountNumber;
        var activationStatus = data.data.activationStatus;
        console.log("Activation Status: " + activationStatus);
        console.log("Card: "+accountNumber);
        if(activationStatus===0){
          $state.go('login.activation');
        }else if(!accountNumber){
          console.log("Going to deposit Card details");
          console.log(JSON.stringify(data.data));
          $state.go('reg.depositCard', {"loginID":data.data.loginID});
        }
      }
    },function(data, status, headers, config){
      //401 invalid access token, back to login
      if(data.status===401){
        $ionicPopup.alert({
          title: "Session Timed out",
          template: "Going back to login screen"
        }).then(function(res){
          window.localStorage.removeItem("accessToken");
          $state.go('login.loginForm');
        });
      }else{
        $ionicPopup.alert({
          title: "Error:",
          template: data.data.result
        });
      }
    });

    //obtain the stored online status.
    $scope.worker.onlineStatus = window.localStorage.getItem("onlineStatus");

    //If no previous stored online status. counted as offline
    if(!$scope.worker.onlineStatus) $scope.worker.onlineStatus = "Offline";

    //If the worker is in online status, show the button "Offline" for the worker.
    if($scope.worker.onlineStatus === "Online" ){
      $scope.worker.toggleOnlineText = "Go Offline";
      $scope.worker.toggleButtonColor = "assertive";
    }else{ //If the worker is in offline status, show the button "Online" for the worker.
      $scope.worker.toggleOnlineText = "Go Online";
      $scope.worker.toggleButtonColor = "balanced";
    }
  };

  //Online/Offline button
  $scope.toggleOnlineStatus = function(){
    //obtain the current online status.
    $scope.worker.onlineStatus = window.localStorage.getItem("onlineStatus");
    //if no previous online status, counted as offline.
    if(!$scope.worker.onlineStatus) $scope.worker.onlineStatus = "Offline";

    //obtain the accesstoken
    var accessToken = window.localStorage.getItem("accessToken");
    //Prepare the object to send to server.
    var workerObj = {
      accessToken: accessToken,
      onlineStatus: $scope.worker.onlineStatus,
      long: 0,
      lat: 0
    }

    var postingOnline = false;

    var posOptions = {maximumAge: 30000, timeout: 5000, enableHighAccuracy: false};

    var onSuccess = function (position) {
      workerObj.long = position.coords.longitude;
      workerObj.lat  = position.coords.latitude;

      if(!postingOnline){
        postingOnline = true;
        //Send to server
        //console.log("Sending to server: ("+workerObj.long+","+workerObj.lat+")");
        $http.post($scope.baseURL + '/onlineWorker', workerObj, {headers: {'Content-Type': 'application/json'}})
        .then(function(data){
          //Success, Change the current online status to Online. Change the button as well.
          $scope.worker.onlineStatus = "Online";
          $scope.worker.toggleOnlineText = "Go Offline";
          $scope.worker.toggleButtonColor = "assertive";
          window.localStorage.setItem("onlineStatus", "Online");
          postingOnline = false;
        },function(data){
          postingOnline = false;
          //401 invalid access token, back to login
          if(data.status===401){
            $ionicPopup.alert({
              title: "Session Timed out",
              template: "Going back to login screen"
            }).then(function(res){
              $scope.worker.onlineStatus = "Offline";
              $scope.worker.toggleOnlineText = "Go Online";
              $scope.worker.toggleButtonColor = "balanced";
              window.localStorage.setItem("onlineStatus", "Offline");
              navigator.geolocation.clearWatch($rootScope.watchID);
              delete $rootScope.watchID;
              window.localStorage.removeItem("accessToken");
              $state.go('login.loginForm');
            });
          }else{
            //Error
            $ionicPopup.alert({
              title: "Error",
              template: data.data.result
            });
          }
          return;
        });
      }
    }

    var onError = function(err) {
      // error
      $scope.showAlert = function() {
        var alertPopup = $ionicPopup.alert({
          title: 'Error',
          template: "Error getting GPS location. Please check your GPS setting."
        });

        alertPopup.then(function(res) {
          console.log("GPS error");
        });
      };
    }
    //If the current status is Online, means the worker wants to "Offline"
    if($scope.worker.onlineStatus === "Online" ){
      //Add offline command to send object.
      workerObj.onlineStatus = "Offline";

      //send to server
      console.log("baseURL: " + $scope.baseURL);
      $http.post($scope.baseURL + '/offlineWorker', workerObj, {headers: {'Content-Type': 'application/json'}})
      .then(function(data){
        //Success, Change the current online status to Offline. Change the button as well.
        $scope.worker.onlineStatus = "Offline";
        $scope.worker.toggleOnlineText = "Go Online";
        $scope.worker.toggleButtonColor = "balanced";
        window.localStorage.setItem("onlineStatus", "Offline");
        navigator.geolocation.clearWatch($rootScope.watchID);
        delete $rootScope.watchID;
      },function(data){
        //401 invalid access token, back to login
        if(data.status===401){
          $ionicPopup.alert({
            title: "Session Timed out",
            template: "Going back to login screen"
          }).then(function(res){
            window.localStorage.removeItem("accessToken");
            $state.go('login.loginForm');
          });
        }else{
          //Error
          $ionicPopup.alert({
            title: "Error",
            template: data.data.result
          });
        }
        return;
      })
    }else{ //If the current status is Offline, means the worker wants to "Online"
      //Add online command to the send object. Also, get the current GPS location(Not implemented)
      workerObj.onlineStatus = "Online";
      if(ionic.Platform.isIOS() || ionic.Platform.isAndroid() || ionic.Platform.isIPad()){
        if(!$rootScope.watchID){
          navigator.geolocation.getCurrentPosition(function(position){
            $rootScope.watchID = navigator.geolocation.watchPosition(onSuccess, onError, posOptions);
          }, function(err){
            var alertPopup = $ionicPopup.alert({
              title: 'Error',
              template: "Error getting GPS location. Please check your GPS setting."
            });

            alertPopup.then(function(res) {
              console.log("GPS error");
            });
          }, {maximumAge: 30000, timeout: 5000, enableHighAccuracy: false})
        }
      }
    }

  };

});
