/*****************************************************************
* Module Name: Login
* Description: User login controller
* Author: Brian Lai
* Date Modified: 2016.01.04
* Related files: login.html
*****************************************************************/
app.controller('loginCtrl', function($scope, $ionicPlatform) {
  $scope.title = "Ahitis Login";

  $scope.load = function(){
    console.log("Login page load");
  }

});
