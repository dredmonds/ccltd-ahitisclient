/*****************************************************************
* Module Name: Payment
* Description: Payment controller
* Author: dredmonds
* Date Modified: 2016.02.03
* Related files: payment.html
*****************************************************************/
app.controller('payment_Ctrl', function($scope, $rootScope, $state, $stateParams, $http, $ionicPopup, $ionicLoading, sha1, $cordovaInAppBrowser) {

$scope.stringToPOST = {};

$scope.load = function(){
  //Obtain the server URL
  $scope.baseURL = window.localStorage.getItem('baseURL');
  $scope.stringToPOST = {};
  $scope.stringToPOST.OrderID = $stateParams.jobID;
  $scope.countryCode = $stateParams.countryCode;

  //Obtain accessToken
  var accessToken = window.localStorage.getItem('accessToken');

  $rootScope.loading = $ionicLoading.show({
    template: 'Loading...',
  });
  if($stateParams.paymentStatus != "Canceled"){
    var skillIDObj = {
    	accessToken: accessToken,
    	skillID: $stateParams.skillID
    };

    //Get skill price
    $http.post($scope.baseURL + '/skill/skillID', skillIDObj, {headers: {'Content-Type': 'application/json'}})
    .then(function(data, status, headers, config){

      if(data.status === 200){
        var skillUnit = data.data.skill.unit;
        var skillPrice = 0;
        for(var priceIdx in $rootScope.prices){
          if($rootScope.prices[priceIdx].skillID === data.data.skill.skillID){
            for(var priceDetailIdx in $rootScope.prices[priceIdx].price){
              var priceObj = $rootScope.prices[priceIdx].price[priceDetailIdx];
              if(priceObj.country === $scope.countryCode){
                skillPrice = (Math.floor(priceObj.pricePerUnit * skillUnit * 100) / 100);
                $scope.stringToPOST.currency = priceObj.currency;
                break;
              }
              if(priceObj.country === "OTH"){
                skillPrice = (Math.floor(priceObj.pricePerUnit * skillUnit * 100) / 100);
                $scope.stringToPOST.currency = priceObj.currency;
              }
            }
          }
        }

        $scope.stringToPOST.buttonID = data.data.skill.liveID;
    	  var jobObj = {
    	    accessToken: accessToken,
    	    jobID: $stateParams.jobID,
          skillPrice: skillPrice
    	  }

        //Job Complete
    	  $http.post($scope.baseURL + '/job/jobComplete', jobObj, {headers: {'Content-Type': 'application/json'}})
        .then(function(data,status,headers,config){
          $rootScope.loading.hide();
    	  	$scope.stringToPOST.hours = data.data.hours + " Hours"; //for Paypal
    	  	$scope.stringToPOST.amount = (skillPrice * data.data.hours) + "00"; //for Barclaycard
    	  },function(data,status,headers,config){
          $rootScope.loading.hide();
          //401 invalid access token, back to login
          if(data.status===401){
            $ionicPopup.alert({
              title: "Session Timed out",
              template: "Going back to login screen"
            }).then(function(res){
              window.localStorage.removeItem("accessToken");
              $state.go('login.loginForm');
            });
          }else{
            //Error Message (Standard)
            $ionicPopup.alert({
              title: "Error:",
              template: data.data.result
            })
          }
    	  });//End of http.post '/job/jobDetail'
      }//End if condition

    },function(data, status, headers, config){
      $rootScope.loading.hide();
      //401 invalid access token, back to login
      if(data.status===401){
        $ionicPopup.alert({
          title: "Session Timed out",
          template: "Going back to login screen"
        }).then(function(res){
          window.localStorage.removeItem("accessToken");
          $state.go('login.loginForm');
        });
      }else{
        //Error Message (Standard)
        $ionicPopup.alert({
          title: "Error:",
          template: data.data.result
        })
      }
    });//End of http.post '/skill/skillID'
  }else{
    var payObj = {
      accessToken: accessToken,
      jobID: $stateParams.jobID
    }

    //Get completed job details
    $http.post($scope.baseURL + '/job/jobPaymentDetail', payObj, {headers: {'Content-Type': 'application/json'}})
    .then(function(data,status,headers,config){
      $rootScope.loading.hide();
      $scope.stringToPOST.hours = data.data.hours + " Units"; //for Paypal
      $scope.stringToPOST.amount = data.data.amount + "00"; //for Barclaycard
    },function(data,status,headers,config){
      $rootScope.loading.hide();
      //401 invalid access token, back to login
      if(data.status===401){
        $ionicPopup.alert({
          title: "Session Timed out",
          template: "Going back to login screen"
        }).then(function(res){
          window.localStorage.removeItem("accessToken");
          $state.go('login.loginForm');
        });
      }else{
        //Error Message (Standard)
        $ionicPopup.alert({
          title: "Error:",
          template: data.data.result
        })
      }
    });//End of http.post '/job/jobDetail'
  }
}//End of function load()


$scope.callPaypal = function(){

	var options = {
		location: 'no',
		clearcache: 'no',
		toolbar: 'yes',
		hidden: 'no',
		hardwareback: 'no'
	};

  //if(ionic.Platform.isIOS() || ionic.Platform.isIPad()){

	var targetURL = '#/transitionPaypal?orderID='+$scope.stringToPOST.OrderID+'&buttonID='+$scope.stringToPOST.buttonID+'&hours='+$scope.stringToPOST.hours.replace(' ', '+') + '&amount='+$scope.stringToPOST.amount + '&currency='+$scope.stringToPOST.currency;

	var URL = encodeURI(targetURL);
	$cordovaInAppBrowser.open(URL, 'paypal', options)
	.then(function(event){
		console.log("Paypal Opened");
		//$state.go("user.main");
	},function(event){
		console.log("Paypal Open Error");
		//$state.go("user.main");
	});

  $state.go('user.jobList', {"scope": "history"});
}

});//End of paymentCtrl
