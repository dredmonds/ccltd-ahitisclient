/*****************************************************************
* Module Name: User's Profile
* Description: User's profile client controller
* Author: dredmonds
* Date Modified: 2015.01.19
* Related files: usrprofile.html
*****************************************************************/
// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'

//Implement the controller profile.js
app.controller('uProfileCtrl', function($scope, $rootScope, $state, $http, $ionicHistory, $ionicPopup, md5) {

	  $scope.logout = function() {
		    var accessToken = window.localStorage.getItem('accessToken');
		    $http.get($scope.baseURL + '/logout?accessToken='+accessToken,{headers: {'Content-Type': 'application/json'}})
		    .then(function(data, status, headers, config){
					window.localStorage.removeItem("accessToken");
		      $state.go('login.loginForm');
		    },function(data, status, headers, config){
					//401 invalid access token, back to login
					if(data.status===401){
						$ionicPopup.alert({
							title: "Session Timed out",
							template: "Going back to login screen"
						}).then(function(res){
							window.localStorage.removeItem("accessToken");
							$state.go('login.loginForm');
						});
					}else{
						//fail
			      $ionicPopup.alert({
			        title: "Error:",
			        template: data.data.result
			      })
					}
		    });
	  };//End of logout function

	  $scope.userEdit = function(){
		  $state.go('user.edit', {"loginID":$scope.loginID});
	  };

	  $scope.editProfile = function(){
		  //$state.go('user.edit');
		  //$scope.closePopover();

		  var promptPopup = $ionicPopup.prompt({
			  title: 'VERIFY ACCOUNT',
			  template: 'Enter your password',
			  inputType: 'password',
			  inputPlaceholder: 'Type your password'
		  });

		  promptPopup.then(function(res){
			  if(res){
				  //console.log('Your input is ',res);
				  var accessToken = window.localStorage.getItem('accessToken');
				  var usrpwdObj = {
						  accessToken: accessToken,
						  password: md5.createHash(res || '')
				  };

				  $http.post($scope.baseURL + '/usrAuthPassword', usrpwdObj, {headers: {'Content-Type': 'application/json'}})
				  .then(function(data){

					  if(data.status == 200){
						  promptPopup.close();
						  $scope.userEdit();
						  //$state.go('user.edit');
					  }

			      },function(data, status, headers, config){
							//401 invalid access token, back to login
              if(data.status===401){
                $ionicPopup.alert({
                  title: "Session Timed out",
                  template: "Going back to login screen"
                }).then(function(res){
                  window.localStorage.removeItem("accessToken");
                  $state.go('login.loginForm');
                });
              }else{
								//error
			          var result = data.data;
			          //403 login fail, wrong password or username
			          if(data.status === 403){
			            var promptAlert = $ionicPopup.alert({
						   			title: 'Error Message',
						   			template: 'Please enter a correct password'
						 			});
			          }else if(result === null){
			              var promptAlert = $ionicPopup.alert({
						     			title: 'Error Message',
						     			template: 'Data record not found'
						   			});
			          }
								promptAlert.then(function(res){
									promptPopup.close();
									promptAlert.close();
									$scope.editProfile();
								});
							}
				  });//End of http post /usrAuthPassword


			  }else if(res == ''){
				  //console.log('Please enter input');
				  var promptAlert = $ionicPopup.alert({
					  title: 'Error Message',
					  template: 'Please enter a password'
				  });
				  promptAlert.then(function(res){
					  promptPopup.close();
					  promptAlert.close();
					  $scope.editProfile();
				  });
			  }//End if res function
		  });//End promptPopup function

	  };//End of Edit Profile function


	$scope.load = function(){
		$scope.baseURL = window.localStorage.getItem('baseURL');
		var accessToken = window.localStorage.getItem('accessToken');
		$scope.account = {}; 			//Initialise account object container to be use

		$http.get($scope.baseURL + '/user?accessToken=' + accessToken, {headers: {'Content-Type': 'application/json'}})
		.then(function(data, status, headers, config){
			//Success
			var result = data.data;
			$scope.loginID = data.data.loginID;
			$scope.account.username = data.data.loginID;
			$scope.account.firstname = data.data.firstname;
			$scope.account.lastname = data.data.lastname;
			$scope.account.email = data.data.email;
			$scope.account.contactNo = data.data.contactNo;
			$scope.account.userType = data.data.userType;
			$scope.account.countryCode = data.data.countryCode;
			$scope.account.createdDate = data.data.createdDate;
			$scope.account.profilePict = data.data.profilePict;

			if($scope.account.profilePict == undefined){
				$scope.account.profilePict = "img/defaultProfilePict.jpg";
			}

		},function(data, status, headers, config){
			//401 invalid access token, back to login
			if(data.status===401){
				$ionicPopup.alert({
					title: "Session Timed out",
					template: "Going back to login screen"
				}).then(function(res){
					window.localStorage.removeItem("accessToken");
					$state.go('login.loginForm');
				});
			}else{
				//Error Message (Standard)
				$ionicPopup.prompt({
					title: "Error:",
					template: data.data.result
				});
			}
		});
	};

})
