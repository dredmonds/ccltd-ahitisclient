/*****************************************************************
* Module Name: Login
* Description: User login controller
* Author: Brian Lai
* Date Modified: 2016.01.04
* Related files: login.html
*****************************************************************/
app.controller('loginFormCtrl', function($rootScope, $scope, $state, $http, $ionicPopup, $ionicLoading, $window, $cordovaDevice, md5) {
  //Prepare the baseURL object.
  $scope.baseURL = {};
  $rootScope.skills = [];

  $scope.login = function(account){
      //Obtain the baseURL object which stored the server address from app.js.
      $scope.baseURL = $rootScope.baseURL;
      $scope.loginURL = $rootScope.loginURL;
      //get the deviceType, will be used for push notification.
      var deviceType = "";
      var deviceUUID = "";
      if(ionic.Platform.isIOS()){
        deviceType = "iOS";
        deviceUUID = $cordovaDevice.getUUID();
      }else if(ionic.Platform.isAndroid()){
        deviceType = "Android";
        deviceUUID = $cordovaDevice.getUUID();
      }else if(ionic.Platform.isIPad()){
        deviceType = "iPad";
        deviceUUID = $cordovaDevice.getUUID();
      }

      console.log("UUID:" + deviceUUID);

      //prepare the login object to send to server.
      var loginObj = {
        username: account.username,
        password: md5.createHash(account.password || ''),
        deviceUUID: deviceUUID,
        deviceType: deviceType
      };
      //login
      console.log("BaseURL: " + $scope.loginURL);
      $rootScope.loading = $ionicLoading.show({
        template: 'Loading...',
      });
      $http.post($scope.loginURL + '/login', loginObj, {headers: {'Content-Type': 'application/json'}})
      .then(function(data){
        //Success
        var result = data.data;
        var accessToken = result.accessToken;
        var userType = result.userType;
        var firstname = result.firstname;
        var lastname = result.lastname;

        //Store the returned accessToken and userType from server to localStorage
        window.localStorage.setItem("accessToken", accessToken);
        window.localStorage.setItem("userType", userType);
        window.localStorage.setItem("deviceType", deviceType);
        window.localStorage.setItem("firstname", firstname);
        window.localStorage.setItem("lastname", lastname);

        //Get resources
        $http.get($scope.baseURL + '/skill/name?accessToken=' + accessToken, {headers: {'Content-Type': 'application/json'}})
        .then(function(data, status, headers, config){
          $rootScope.loading.hide();
          //success
          var result = data.data;
          for(var idx in data.data.skills){
            var skillObj = {};
    			  skillObj.skillid = data.data.skills[idx].skillid;
            skillObj.name = data.data.skills[idx].name;
            skillObj.icon = data.data.skills[idx].icon;
            $rootScope.skills.push(skillObj);
          }

          //returned as worker
          if(userType === "Worker"){
            console.log("Going to worker main");
            $state.go('worker.main');
          }else{ //returned as user
            console.log("Going to user main");
            $state.go('user.main');
          }

        },function(data, status, headers, config){
          $rootScope.loading.hide();
          //401 invalid access token, back to login
          if(data.status===401){
            $ionicPopup.alert({
              title: "Session Timed out",
              template: "Going back to login screen"
            }).then(function(res){
              window.localStorage.removeItem("accessToken");
              $state.go('login.loginForm');
            });
          }
          //fail
          $ionicPopup.alert({
            title: "Error:",
            template: data.data.result
          })
        });

        $http.get($scope.baseURL + '/price?accessToken=' + accessToken, {headers: {'Content-Type': 'application/json'}})
        .then(function(data,status,headers,config){
          $rootScope.loading.hide();
          //Success
          $rootScope.prices = [];
          for(var idx in data.data.prices){
            var priceObj = {};
            priceObj.skillID = data.data.prices[idx].skillID;
            priceObj.price = data.data.prices[idx].price;
            $rootScope.prices.push(priceObj);
          }
        }, function(data,status,headers,config){
          $rootScope.loading.hide();
          //401 invalid access token, back to login
          if(data.status===401){
            $ionicPopup.alert({
              title: "Session Timed out",
              template: "Going back to login screen"
            }).then(function(res){
              window.localStorage.removeItem("accessToken");
              $state.go('login.loginForm');
            });
          }else{
            //fail
            $ionicPopup.alert({
              title: "Error:",
              template: data.data.result
            })
          }
        });
      },function(data, status, headers, config){
        $rootScope.loading.hide();
        //error
        var result = data.data;
        var title = "";
        //403 login fail, wrong password or username
        if(data.status === 403){
          title = "Login Fail";
        }else{
          if(result === null){
            console.log(JSON.stringify({data: data, status: status, headers: headers, config: config}));
            $ionicPopup.alert({
              title: "Error",
              template: "Connection timeout. Please check your connection."
            });
            return;
          }
          title = "Error";
        }
        console.log(JSON.stringify({data: data, status: status, headers: headers, config: config}));
        $ionicPopup.alert({
          title: title,
          template: data.data.result
        });
      });
  }
})
