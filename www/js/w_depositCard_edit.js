/*****************************************************************
* Module Name: Edit Bank Details and DBS Number
* Description: Update Bank Details and Disclosure and Barring Service(DBS) Number
* Author: dredmonds
* Date Modified: 2016.07.08
* Related files: w_depositCard_edit.html
*****************************************************************/

//Add a new directive to use in html. it will pass to compareTo function above.
app.controller('wDepositCardCtrl', function($scope, $rootScope, $state, $stateParams, $http, $ionicPopup, $ionicLoading, $cordovaInAppBrowser) {

  $scope.openWindow = function(){
    var options = {
      location: 'yes',
      toolbar: 'yes',
    };

	var targetURL = "https://www.gov.uk/government/organisations/disclosure-and-barring-service/about";
	var URL = encodeURI(targetURL);
	$cordovaInAppBrowser.open(URL, '_system', options)
	.then(function(event){
		console.log("DBS Information window open.");
	},function(event){
		console.log("Error opening for DBS Information page.");
	});
  };//End of openWindow function

  //Prepare the objects
  $scope.account = {
    userType: "Worker"
  };
  $scope.baseURL = {};
  $scope.years = [];


  //load values on textbox
  $scope.load = function(){
	console.log("Loading bank details and dbs no.");
	$scope.baseURL = window.localStorage.getItem('baseURL');
	var accessToken = window.localStorage.getItem('accessToken');
	$scope.account = {}; 			//Initialise account object container to be use

	$http.get($scope.baseURL + '/register/getDepositCard?accessToken=' + accessToken, {headers: {'Content-Type': 'application/json'}})
	.then(function(data, status, headers, config){
		//Success
		$scope.account.DBSref             = data.data.DBSref;
        $scope.account.cardName           = data.data.cardName;
        $scope.account.cardAddress        = data.data.cardAddress;
        $scope.account.cardCity           = data.data.cardCity;
        $scope.account.cardPostCode       = data.data.cardPostCode;
        $scope.account.cardBirthDay       = data.data.cardDay.toString();
        $scope.account.cardBirthMonth     = data.data.cardMonth.toString();
        $scope.account.cardBirthYear      = data.data.cardYear.toString();
        $scope.account.cardBankName       = data.data.cardBankName;
        $scope.account.cardAccountNumber  = data.data.cardAccountNumber;
        $scope.account.cardSortCode       = data.data.cardSortCode;
        $scope.account.cardIBAN           = data.data.cardIBAN;
        $scope.account.cardSwiftCode      = data.data.cardSwiftCode;

	},function(data, status, headers, config){
    //401 invalid access token, back to login
    if(data.status===401){
      $ionicPopup.alert({
        title: "Session Timed out",
        template: "Going back to login screen"
      }).then(function(res){
        window.localStorage.removeItem("accessToken");
        $state.go('login.loginForm');
      });
    }else{
      //Error Message (Standard)
      $ionicPopup.prompt({
        title: "Error:",
        template: data.data.result
      })
    }
	});


    for(var i = 1950;i<=2016;i++){
      $scope.years.push(i.toString());
    }//populate textbox menu


  };//end of load values function



  //update Bank Details
  $scope.updateBankDetail = function(account){
	  console.log("Updating bank details and dbs no.");
      //server URL
      $scope.baseURL = window.localStorage.getItem('baseURL');
      $scope.accessToken = window.localStorage.getItem('accessToken');
      //prepare the register object to send to server.
      var regObj = {
    	DBSref: account.DBSref,
        cardName: account.cardName,
        cardAddress: account.cardAddress,
        cardCity: account.cardCity,
        cardPostCode: account.cardPostCode,
        cardDay: account.cardBirthDay,
        cardMonth: account.cardBirthMonth,
        cardYear: account.cardBirthYear,
        cardBankName: account.cardBankName,
        cardAccountNumber: account.cardAccountNumber,
        cardSortCode: account.cardSortCode,
        cardIBAN: account.cardIBAN,
        cardSwiftCode: account.cardSwiftCode,
        accessToken: $scope.accessToken
      };
      $rootScope.loading = $ionicLoading.show({
        template: 'Loading...',
      });
      //register
      $http.post($scope.baseURL + '/register/updateDepositCard', regObj, {headers: {'Content-Type': 'application/json'}})
      .then(function(data, status, headers, config){
        $rootScope.loading.hide();
        //success
        var result = data.data;
        $ionicPopup.alert({
          title: "Update Worker Bank Details and DBS Number",
          template: result.result
        }).then(function(res){
          console.log("Back to profile.");
          $state.go('worker.profile');
        });
      },function(data, status, headers, config){
        $rootScope.loading.hide();
        //401 invalid access token, back to login
        if(data.status===401){
          $ionicPopup.alert({
            title: "Session Timed out",
            template: "Going back to login screen"
          }).then(function(res){
            window.localStorage.removeItem("accessToken");
            $state.go('login.loginForm');
          });
        }else{
          //Error
          $ionicPopup.alert({
            title: "Error:",
            template: data.data.result
          });
        }
      });
  };//end of updateBankDetail function

});//End of app.controller
