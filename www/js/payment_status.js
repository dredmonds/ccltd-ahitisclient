/*****************************************************************
* Module Name: Payment Status
* Description: Payment status controller
* Author: dredmonds
* Date Modified: 2016.05.13
* Related files: payment_status.html
*****************************************************************/
app.controller('paymentStatus_Ctrl', function($scope, $ionicPopup, $stateParams, $cordovaDevice) {
	

	$scope.proceedJobList = function(){
		$state.go('user.jobList');
	}//End of function proceedJobList()

    $scope.load = function(){               //Extract values from redirection page
      $scope.payment = {};
      $scope.payment.reference = $stateParams.jobID;
      $scope.payment.jobID = $stateParams.txnID;
      $scope.payment.amount = $stateParams.amt;
      $scope.payment.status = $stateParams.status;
    }//End of function load()

});//End of paymentStatus_Ctrl
