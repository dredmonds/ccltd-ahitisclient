/*****************************************************************
* Module Name: User's Job Detail
* Description: User's Job Detail controller
* Author: Brian Lai
* Date Modified: 2016.05.06
* Related files: u_jobDetail.html
*****************************************************************/
// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'

//Implement the controller profile.js

app.controller('uJobDetailCtrl', function($scope, $rootScope, $http, $state, $ionicPopup, $window) {
  $scope.job = {};

	$scope.load = function(){
		$scope.baseURL = window.localStorage.getItem('baseURL');
		var accessToken = window.localStorage.getItem('accessToken');
    var deviceType = "";
    if(ionic.Platform.isIPad()){
      deviceType = "iPad";
    }else if(ionic.Platform.isAndroid()){
      deviceType = "android";
    }else{
      deviceType = "others";
    }

//    if($rootScope.job){
//      $scope.job = $rootScope.job;
//      $rootScope.jobID = $scope.job.jobID;
//      initJobMap($scope.job.loc[1],$scope.job.loc[0],deviceType);
//    }else if($rootScope.jobID){
      if($rootScope.job){
         $scope.job = $rootScope.job;
         $rootScope.jobID = $scope.job.jobID;
      }
      var jobReqObj = {
          accessToken: accessToken,
          jobID: $rootScope.jobID
      };

      $http.post($scope.baseURL + '/job/jobDetail', jobReqObj, {headers: {'Content-Type': 'application/json'}})
      .then(function(data){

        if(data.status == 200){
          var jobObj = {};
          jobObj.jobID = data.data.job.jobID;
          jobObj.userID = data.data.job.userID;
          jobObj.workerID = data.data.job.workerID;
          jobObj.skillID = data.data.job.skillID;
          jobObj.countryCode = data.data.job.countryCode;

          for(var skillIdx in $rootScope.skills){
            if($rootScope.skills[skillIdx].skillid === data.data.job.skillID){
              jobObj.skill = $rootScope.skills[skillIdx].name;
              jobObj.skillIcon = $rootScope.skills[skillIdx].icon;
            }
          }
          jobObj.loc = data.data.job.loc;

          jobObj.statusID = data.data.job.statusID;
          switch(data.data.job.statusID){
            case 1:
              jobObj.status = "Waiting for acceptance";
              break;
            case 2:
              jobObj.status = "In progress";
              break;
            case 3:
              jobObj.status = "Completed";
              break;
            case 4:
              jobObj.status = "In dispute";
              break;
            case 5:
              jobObj.status = "Rejected";
              break;
            default:
              break;
          }

          jobObj.paymentStatusID = data.data.job.paymentStatusID;
          switch(data.data.job.paymentStatusID){
            case 1:
              jobObj.paymentStatus = "Pending";
              break;
            case 2:
              jobObj.paymentStatus = "Completed";
              break;
            case 3:
              jobObj.paymentStatus = "Canceled";
              break;
            default:
              break;
          }

          jobObj.createdDate = data.data.job.createdDate.replace('T', ' ').replace('Z', ' ');

          var workerReqObj = {
              accessToken: accessToken,
              workerID: jobObj.workerID
          };

          $http.post($scope.baseURL + '/worker/getWorker', workerReqObj, {headers: {'Content-Type': 'application/json'}})
          .then(function(data){

            if(data.status == 200){
              jobObj.workerIcon = data.data.profilePict;
              jobObj.contactNumber = data.data.contactNo;

              $scope.job = jobObj;
              initJobMap($scope.job.loc[1],$scope.job.loc[0],deviceType);
            }else{
              console.log(JSON.stringify(data));
            }
          }, function(data, status, headers, config){
            if(data.status===401){
              $ionicPopup.alert({
                title: "Session Timed out",
                template: "Going back to login screen"
              }).then(function(res){
                window.localStorage.removeItem("accessToken");
                $state.go('login.loginForm');
              });
            }else{
              //fail
              $ionicPopup.alert({
                title: "Error:",
                template: data.data.result
              })
            }
          });

        }
      }, function(data, status, headers, config){
        //401 invalid access token, back to login
        if(data.status===401){
          $ionicPopup.alert({
            title: "Session Timed out",
            template: "Going back to login screen"
          }).then(function(res){
            window.localStorage.removeItem("accessToken");
            $state.go('login.loginForm');
          });
        }else{
          //fail
          $ionicPopup.alert({
            title: "Error:",
            template: data.data.result
          })
        }
      });

//    }
	};

  //job complete
  $scope.jobComplete = function(){
    //to Payment
    $state.go('user.payment.main', {"jobID": $scope.job.jobID, "skillID": $scope.job.skillID, "countryCode": $scope.job.countryCode, "paymentStatus" : "Pending"});
  }

  //job dispute
  $scope.jobDispute = function(){
    //to dispute page
    $state.go("user.help.disputes", {"jobID":$scope.job.jobID});
  }

  //Pay
  $scope.jobPayment = function(){
    //to Payment
    $state.go('user.payment.main', {"jobID": $scope.job.jobID, "skillID": $scope.job.skillID, "paymentStatus" : "Canceled"});
  }
});
