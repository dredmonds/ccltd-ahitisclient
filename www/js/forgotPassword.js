/*****************************************************************
* Module Name: Forgot password
* Description: Forgot Password controller
* Author: Brian Lai
* Date Modified: 2016.01.04
* Related files: forgotPassword.html
*****************************************************************/
app.controller('forgotPasswordCtrl', function($scope, $rootScope, $ionicLoading, $state, $http, $ionicPopup, $window, $cordovaDevice) {
  //Prepare the baseURL object.
  $scope.baseURL = {};

  $scope.forgotPassword = function(account){
      //Obtain the baseURL object which stored the server address from app.js.
      $scope.baseURL = window.localStorage.getItem('baseURL');

      //prepare the forgotPassword object to send to server.
      var forgotPasswordObj = {
        username: account.username
      };
      //login
      console.log("BaseURL" + $scope.baseURL);
      $rootScope.loading = $ionicLoading.show({
        template: 'Loading...',
      });
      $http.post($scope.baseURL + '/forgotPassword', forgotPasswordObj, {headers: {'Content-Type': 'application/json'}})
      .then(function(data){
        $rootScope.loading.hide();
        //Success
        var result = data.data;

        $ionicPopup.alert({
          title: "Forgot password",
          template: "Verification email has been sent to your email address. Please use the verification code in the email to reset your password."
        });

        $state.go('login.resetPassword');
      },function(data, status, headers, config){
        //error
        $rootScope.loading.hide();
        var result = data.data;
        var title = "";

        //403 login fail, wrong password or username
        if(data.status === 403){
          title = "Login Fail";
        }else{
          if(result === null){
            console.log(JSON.stringify({data: data, status: status, headers: headers, config: config}));
            $ionicPopup.alert({
              title: "Error",
              template: "Connection timeout. Please check your connection."
            });
            return;
          }
          title = "Error";
        }
        console.log(JSON.stringify({data: data, status: status, headers: headers, config: config}));
        $ionicPopup.alert({
          title: title,
          template: result.result
        });
      });
  }
})
