/*****************************************************************
* Module Name: User's Job List
* Description: User's Job List controller
* Author: Brian Lai
* Date Modified: 2016.05.04
* Related files: u_jobList.html
*****************************************************************/
// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'

//Implement the controller profile.js
app.controller('uJobListCtrl', function($scope, $rootScope, $http, $state, $stateParams, $ionicPopup, $window) {
  $scope.jobs = [];

	$scope.load = function(){
		$scope.baseURL = window.localStorage.getItem('baseURL');
		var accessToken = window.localStorage.getItem('accessToken');
    $scope.scope = $stateParams.scope;
    if($scope.scope === "dispute"){
      $scope.titleText = "Please select a job you want to dispute.";
      if(ionic.Platform.isIOS() || ionic.Platform.isIPad()){
        $scope.spacer = "height: 148px;";
      }else{
        $scope.spacer = "height: 128px;";
      }
      $scope.innerSpacer = "height:246px;";
    }else{
      $scope.titleText = "Please select a job";
      if(ionic.Platform.isIOS() || ionic.Platform.isIPad()){
        $scope.innerSpacer = "height:98px;";
      }else{
        $scope.innerSpacer = "height:118px;";
      }
    }
    console.log("Scope="+$stateParams.scope);
    //Get jobs
      $http.get($scope.baseURL + '/job/userList?accessToken=' + accessToken + '&scope=' + $stateParams.scope, {headers: {'Content-Type': 'application/json'}})
      .then(function(data, status, headers, config){
        var jobResult = data.data;
        for(var idx in data.data.jobs){
          var jobObj = {};
          jobObj.jobID = data.data.jobs[idx].jobID;
          jobObj.userID = data.data.jobs[idx].userID;
          jobObj.workerID = data.data.jobs[idx].workerID;
          jobObj.skillID = data.data.jobs[idx].skillID;

          for(var skillIdx in $rootScope.skills){
            if($rootScope.skills[skillIdx].skillid === data.data.jobs[idx].skillID){
              jobObj.skill = $rootScope.skills[skillIdx].name;
              jobObj.skillIcon = $rootScope.skills[skillIdx].icon;
            }
          }
          jobObj.loc = data.data.jobs[idx].loc;

          jobObj.statusID = data.data.jobs[idx].statusID;
          switch(data.data.jobs[idx].statusID){
            case 1:
              jobObj.status = "Waiting for acceptance";
              break;
            case 2:
              jobObj.status = "In progress";
              break;
            case 3:
              jobObj.status = "Completed";
              break;
            case 4:
              jobObj.status = "In dispute";
              break;
            case 5:
              jobObj.status = "Rejected";
              break;
            default:
              break;
          }

          jobObj.paymentStatusID = data.data.jobs[idx].paymentStatusID;
          switch(data.data.jobs[idx].paymentStatusID){
            case 1:
              jobObj.paymentStatus = "Pending";
              break;
            case 2:
              jobObj.paymentStatus = "Completed";
              break;
            case 3:
              jobObj.paymentStatus = "Canceled";
              break;
            default:
              break;
          }

          jobObj.createdDate = data.data.jobs[idx].createdDate.toString().replace("T"," ").replace("Z", " ");

          $scope.jobs.push(jobObj);
        }
      }, function(data, status, headers, config){
        //401 invalid access token, back to login
        if(data.status===401){
          $ionicPopup.alert({
            title: "Session Timed out",
            template: "Going back to login screen"
          }).then(function(res){
            window.localStorage.removeItem("accessToken");
            $state.go('login.loginForm');
          });
        }else{
          //fail
          if(data.data){
            $ionicPopup.alert({
              title: "Error:",
              template: data.data.result
            })
          }
        }
      });
	};

  $scope.selectedJob = function(jobID){
    if($scope.scope==="dispute"){
      $state.go("user.help.disputes", {"jobID":jobID});
    }else{
      //go to job Detail
      for(var idx in $scope.jobs){
        if($scope.jobs[idx].jobID === jobID){
          $rootScope.job = $scope.jobs[idx];
          break;
        }
      }
      $rootScope.jobID = jobID;
      $state.go("user.jobDetail");
    }
  };

})
