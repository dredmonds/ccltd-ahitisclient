/*****************************************************************
* Module Name: search worker
* Description: search ahitis worker by user controller
* Author: Brian Lai
* Date Modified: 2016.02.05
* Related files: search_worker.html
*****************************************************************/

//Add a new directive to use in html. it will pass to compareTo function above.
app.controller('searchWorkerCtrl', function($scope, $rootScope, $state, $http, $timeout, $window, $ionicPopup, $ionicLoading) {

  $scope.durationList = [{text:"2 Hours",value:"2"},
                         {text:"3 Hours",value:"3"},
                         {text:"4 Hours",value:"4"},
                         {text:"5 Hours",value:"5"},
                         {text:"6 Hours",value:"6"},
                         {text:"7 Hours",value:"7"},
                         {text:"8 Hours",value:"8"}];

  $scope.locationTypeList = [{text:"Current location",value:"GPS"},
                             {text:"Preferred location",value:"MAP"}];

  $scope.initMap = function(){
      initMap($rootScope.searchCriteria.location.lat,$rootScope.searchCriteria.location.long);
  }

  $scope.load = function(){
    $scope.baseURL = window.localStorage.getItem('baseURL');
		var accessToken = window.localStorage.getItem('accessToken');
    $scope.skills = [];

    $rootScope.loading = $ionicLoading.show({
      template: 'Loading...',
    });

    //get user location for the price
    var posOptions = {timeout: 5000, enableHighAccuracy: false};
    var onSuccess = function (position) {
      console.log("Position retrieved: (" + position.coords.longitude + ", " + position.coords.latitude + ")");

      var postObj = {
        lat: position.coords.latitude,
        long: position.coords.longitude,
        accessToken: accessToken
      }

      $http.post($scope.baseURL + '/user/countryCode',postObj, {headers: {'Content-Type': 'application/json'}})
      .then(function(data, status, headers, config){
        var result = data.data;
        var countryCode = result.countryCode;
        $rootScope.searchCriteria.countryCode = countryCode;

        //get skill List from server.
        $http.get($scope.baseURL + '/skill/name?accessToken=' + accessToken, {headers: {'Content-Type': 'application/json'}})
        .then(function(data, status, headers, config){
          //success
          $rootScope.loading.hide();

          var result = data.data;
          for(var idx in data.data.skills){
            var skillObj = {};
    			  skillObj.skillid = data.data.skills[idx].skillid;
            skillObj.name = data.data.skills[idx].name;
            for(var priceIdx in $rootScope.prices){
              if($rootScope.prices[priceIdx].skillID === skillObj.skillid){
                for(var priceDetailIdx in $rootScope.prices[priceIdx].price){
                  var priceObj = $rootScope.prices[priceIdx].price[priceDetailIdx];

                  if(priceObj.country === countryCode){
                    skillObj.price = priceObj.currencyLbl + (Math.floor(priceObj.pricePerUnit * data.data.skills[idx].unit * 100) / 100) + " / Hour";
                    break;
                  }
                  if(priceObj.country === "OTH"){
                    skillObj.price = priceObj.currencyLbl + (Math.floor(priceObj.pricePerUnit * data.data.skills[idx].unit * 100) / 100) + " / Hour";
                  }
                }
              }
            }
            skillObj.icon = data.data.skills[idx].icon;
            $scope.skills.push(skillObj);
          }

        },function(data, status, headers, config){
          //401 invalid access token, back to login
          if(data.status===401){
            $ionicPopup.alert({
              title: "Session Timed out",
              template: "Going back to login screen"
            }).then(function(res){
              window.localStorage.removeItem("accessToken");
              $state.go('login.loginForm');
            });
          }else{
            //fail
            $ionicPopup.alert({
              title: "Error:",
              template: data.data.result
            })
          }
        });

      },function(data,status,headers,config){
        if(data.status===401){
          $ionicPopup.alert({
            title: "Session Timed out",
            template: "Going back to login screen"
          }).then(function(res){
            window.localStorage.removeItem("accessToken");
            $state.go('login.loginForm');
          });
        }else{
          //error
          $ionicPopup.alert({
            title: "Error:",
            template: data.data.result
          });
        }
      });
    };
    var onError = function(err) {
      // error
        var alertPopup = $ionicPopup.alert({
          title: 'Error',
          template: "Error getting GPS location. Please check your GPS setting."
        });

        alertPopup.then(function(res) {
          console.log("GPS error");
        });
    };
    navigator.geolocation.getCurrentPosition(onSuccess, onError, posOptions);

  };

  $scope.selectedSkill = function(skillID){
    //go to duration
    $rootScope.searchCriteria.skillID = skillID;
    $state.go("user.search.duration");
  };

  $scope.selectedDuration = function(duration){
    //go to locationType
    $rootScope.searchCriteria.duration = duration.value;
    var deviceType = window.localStorage.getItem("deviceType");
    if(deviceType === "NA"){
      $rootScope.searchCriteria.location = {};
      $rootScope.searchCriteria.location.lat = 51.508742;
      $rootScope.searchCriteria.location.long = -0.120850;
      $state.go("user.search.map");
    }else{
      $state.go("user.search.locationType");
    }
  };

  $scope.selectedLocationType = function(locationType){
    //send to server by GPS or open MAP
    //Get GPS location
      var posOptions = {timeout: 5000, enableHighAccuracy: false};
      var onSuccess = function (position) {
        console.log("Position retrieved: (" + position.coords.longitude + ", " + position.coords.latitude + ")");
        $rootScope.searchCriteria.location = {};
        $rootScope.searchCriteria.location.lat  = position.coords.latitude;
        $rootScope.searchCriteria.location.long = position.coords.longitude;

        if(locationType.value === "GPS"){
          searchWorker();
        }else{ //Open Map
          $state.go("user.search.map");
        }
      };
      var onError = function(err) {
        // error
          var alertPopup = $ionicPopup.alert({
            title: 'Error',
            template: "Error getting GPS location. Please check your GPS setting."
          });

          alertPopup.then(function(res) {
            console.log("GPS error");
          });
      };
      navigator.geolocation.getCurrentPosition(onSuccess, onError, posOptions);
  };

  $scope.confirmMap = function(){
    searchWorker();
  }

  var searchWorker = function(){
    $rootScope.loading = $ionicLoading.show({
      template: 'Searching AHitis workers...',
    });
    $scope.baseURL = window.localStorage.getItem('baseURL');
    //to server
    var accessToken = window.localStorage.getItem('accessToken');
    $rootScope.searchCriteria.accessToken = accessToken;
    $http.post($scope.baseURL + '/worker/search', $rootScope.searchCriteria, {headers: {'Content-Type': 'application/json'}})
    .then(function(data, status, headers, config){
      $rootScope.loading.hide();
      if(data.status === 200){
        //Success
        var jobID = data.data.jobID;

        $ionicPopup.alert({
          title: "Search workers",
          template: data.data.workers + " AHitis workers found. Waiting for workers acceptance."
        }).then(function(res){

        });

        $timeout(function(){
          var jobReqObj = {
              accessToken: window.localStorage.getItem('accessToken'),
              jobID: jobID
          };

          $http.post($scope.baseURL + '/job/jobReject', jobReqObj, {headers: {'Content-Type': 'application/json'}})
          .then(function(data2){

            if(data2.status === 200 && data2.data.result === "Rejected"){
              $cordovaDialogs.alert("Job Rejected", "Job");
              $rootScope.jobID = jobID;
              $state.go("user.main");
            }
          }, function(data2, status2, headers2, config2){
            if(data.status===401){
              $ionicPopup.alert({
                title: "Session Timed out",
                template: "Going back to login screen"
              }).then(function(res){
                window.localStorage.removeItem("accessToken");
                $state.go('login.loginForm');
              });
            }else{
              //fail
              $ionicPopup.alert({
                title: "Error:",
                template: JSON.stringify({data: data2, status: status2, headers: headers2, config: config2})
              });
            }
          });

        },30000);

        $state.go("user.main");
      }else if(data.status === 204){
        $ionicPopup.alert({
          title: "Ahitis worker",
          template: "No Ahitis workers suitable around you."
        });
      }
    },function(data, status, headers, config){
      $rootScope.loading.hide();
      //401 invalid access token, back to login
      if(data.status===401){
        $ionicPopup.alert({
          title: "Session Timed out",
          template: "Going back to login screen"
        }).then(function(res){
          window.localStorage.removeItem("accessToken");
          $state.go('login.loginForm');
        });
      }else{
        //error
        $ionicPopup.alert({
          title: "Error:",
          template: data.data.result
        });
      }
    });
  }
});
