/*****************************************************************
* Module Name: Register
* Description: User register controller
* Author: Brian Lai
* Date Modified: 2016.01.04
* Related files: register.html
*****************************************************************/

//Add a new directive to use in html. it will pass to compareTo function above.
app.controller('regFormCtrl', function($scope, $rootScope, $state, $http, $ionicPopup, $ionicLoading, md5) {
  //Prepare the objects
  $scope.account = {
    userType: "User"
  };
  $scope.baseURL = {};

  //register button
  $scope.reg = function(account){
    //server URL
      $scope.loginURL = window.localStorage.getItem('loginURL');
      //prepare the register object to send to server.
      var regObj = {
        username: account.username,
        password: md5.createHash(account.password || ''),
        firstname: account.firstname,
        lastname: account.lastname,
        email: account.email,
        contactNo: account.contactNo,
        userType: account.userType,
        countryCode: account.countryCode,
        skill: account.skill,
        depositCard: "",
        depositCardPic: ""
      };
      $rootScope.loading = $ionicLoading.show({
        template: 'Loading...',
      });
      //register
      $http.post($scope.loginURL + '/register', regObj, {headers: {'Content-Type': 'application/json'}})
      .then(function(data, status, headers, config){
        $rootScope.loading.hide();
        //success
        var result = data.data;
        $ionicPopup.alert({
          title: "Register",
          template: result.result
        });
        $state.go("login.loginForm");
      },function(data, status, headers, config){
        $rootScope.loading.hide();
        //Error
        $ionicPopup.alert({
          title: "Error:",
          template: data.data.result
        })
      });
  };


});
