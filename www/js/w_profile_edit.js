/*****************************************************************
* Module Name: Edit Worker Profile
* Description: Edit worker profile controller
* Author: dredmonds
* Date Modified: 2016.01.19
* Related files: w_profile_edit.html
*****************************************************************/

app.controller('wProfileEditCtrl', function($scope, $rootScope, $state, $stateParams, $http, $ionicPopup, $ionicLoading, md5) {

	  $scope.baseURL = {};
	  //$scope.account = {profilePict:""};
	  var pictureSource;   // picture source
	  var destinationType; // sets the format of returned value


	  $scope.cameraCapture = function(){
	    //console.log("got camera button click");
	    var options =   {
	    quality: 50,
	    destinationType: destinationType,
	    sourceType: pictureSource,
	    encodingType: 0
	    };

	    if (!navigator.camera){
	      //error handling
	      return;
	    }

	    navigator.camera.getPicture(
	      function (imageURI) {
	        //console.log("got camera success ", imageURI);
	        $scope.baseURL = window.localStorage.getItem('baseURL');
	        var accessToken = window.localStorage.getItem('accessToken');
	        $scope.mypicture = imageURI;
					$scope.account.profilePict = imageURI;
	        $scope.uploadurl = $scope.baseURL + "/uploads/profilePict";
	        if (!$scope.uploadurl){
	    	  // error handling no upload url
	    	  return;
	    	}
	    	if (!$scope.mypicture){
	    	  // error handling no picture given
	    	  return;
	    	}

	    	var options = new FileUploadOptions();
	    	options.fileKey="profilePict";
	    	options.fileName=$scope.mypicture.substr($scope.mypicture.lastIndexOf('/')+1);
	    	options.mimeType="image/jpeg";
	        options.chunkedMode = false;
	        options.headers = {
	          Connection: "close"
	        };
	        options.params = {};
	        console.log($stateParams.loginID);
	        options.params = {
	          "loginID": $stateParams.loginID,
	          "fileName": options.fileName
	        };



	    	//var params = {};
	    	//params.other = obj.text; // some other POST fields
	    	//options.params = params;


	    	//console.log("new imp: prepare upload now");
	    	var ft = new FileTransfer();

			$rootScope.loading = $ionicLoading.show({
	          template: 'Uploading...' + $stateParams.loginID,
	        });

			ft.upload($scope.mypicture, encodeURI($scope.uploadurl), uploadSuccess, uploadError, options, true);

			  function uploadSuccess(r) {
							$scope.account.profilePict = $scope.baseURL + "/uploads/profilePict/" + $stateParams.loginID + "-" + options.fileName;
	            $rootScope.loading.hide();
	            console.log("Upload success");
	    	  }

	    	  function uploadError(error) {
	    	    console.log("upload error source " + error.source);
	    	    console.log("upload error target " + error.target);
	            console.log(JSON.stringify(error));
	            $rootScope.loading.hide();
	    	  }

	      },
	      function (err) {
	        //console.log("got camera error ", err);
	        // error handling camera plugin
	      },
	      options);

	  };//End of takeProfile function



/*	  $scope.saveImage = function(){

	    //server URL
	    $scope.baseURL = window.localStorage.getItem('baseURL');
	    $scope.accessToken = window.localStorage.getItem('accessToken');

	    //prepare the register object to send to server.
	    var regObj = {
	      //depositCard: account.depositCard,
	      profilePict: $scope.account.profilePict,
	      accessToken: $scope.accessToken
	    };

		//register
	    $http.post($scope.baseURL + '/register/saveProfilePict', regObj, {headers: {'Content-Type': 'application/json'}})
	    .then(function(data, status, headers, config){
	      //success
	      var result = data.data;
	      $state.go("worker.edit");
	    },function(data, status, headers, config){
	      //Error
	      $ionicPopup.alert({
	        title: "Error:",
	        template: data.data.result
	      });
	    });

	  };//End of upload function
*/


	  //Save edit password button
	  $scope.editSave = function(pwd){
		  $scope.active = true;
	      passwordHash = md5.createHash($scope.pwd.newpassword || '');

	      //server URL
	      $scope.baseURL = window.localStorage.getItem('baseURL');
	      var accessToken = window.localStorage.getItem('accessToken');

	      //prepare the user object to send to server.
	      var wkrObj = {
	    	accessToken: accessToken,                      /*insert accessToken as payload to validates*/
	        username: $scope.account.username,                    /*No rules yet, if included for updates*/
	        password: passwordHash,
	        firstname: $scope.account.firstname,
	        lastname: $scope.account.lastname,
	        email: $scope.account.email,
	        contactNo: $scope.account.contactNo,
	        //userType: account.userType,                    /*No rules yet, if included for updates*/
	        countryCode: $scope.account.countryCode,
	        skillID: $scope.account.skillID,
	        profilePict: $scope.account.profilePict
	      };
	      //Update user profile
	      $http.post($scope.baseURL + '/worker', wkrObj, {headers: {'Content-Type': 'application/json'}})
	      .then(function(data, status, headers, config){
	          //success
	          var result = data.data;
	          $scope.account.password = passwordHash;

	          $ionicPopup.alert({
	            title: "Update Worker Password",
	            template: result.result
	          })
	      },function(data, status, headers, config){
					//401 invalid access token, back to login
					if(data.status===401){
						$ionicPopup.alert({
							title: "Session Timed out",
							template: "Going back to login screen"
						}).then(function(res){
							window.localStorage.removeItem("accessToken");
							$state.go('login.loginForm');
						});
					}else{
						//Error Message (Standard)
			        $ionicPopup.prompt({
			          title: "Error:",
			          template: data.data.result
			        });
					}
	      });

	  };

	  //Cancel edit password button
	  $scope.editCancel = function(){
		  $scope.active = true;
		  $scope.pwd = {newpassword:"", confirmnewpassword:""};
	  };

  /*Start of User's Profile save button*/
  $scope.wkrprfl = function(account){

	  $scope.active = true;
	  //server URL
      $scope.baseURL = window.localStorage.getItem('baseURL');
	  var accessToken = window.localStorage.getItem('accessToken');

      //prepare the user object to send to server.
      var wkrObj = {
    	accessToken: accessToken,                      /*insert accessToken as payload to validates*/
        username: account.username,                    /*No rules yet, if included for updates*/
        password: account.password,
        firstname: account.firstname,
        lastname: account.lastname,
        email: account.email,
        contactNo: account.contactNo,
        //userType: account.userType,                    /*No rules yet, if included for updates*/
        countryCode: account.countryCode,
        skillID: account.skillID,
        profilePict: account.profilePict
      };
			$rootScope.loading = $ionicLoading.show({
	      template: 'Loading...',
	    });
      //Update user profile
      $http.post($scope.baseURL + '/worker', wkrObj, {headers: {'Content-Type': 'application/json'}})
      .then(function(data, status, headers, config){
				$rootScope.loading.hide();
        //success
        var result = data.data;
        $ionicPopup.alert({
          title: "Update Worker Profile",
          template: result.result
        }).then(function(res){
          console.log("Back to profile.");
          $state.go('worker.profile');
        });
      },function(data, status, headers, config){
				$rootScope.loading.hide();
				//401 invalid access token, back to login
				if(data.status===401){
					$ionicPopup.alert({
						title: "Session Timed out",
						template: "Going back to login screen"
					}).then(function(res){
						window.localStorage.removeItem("accessToken");
						$state.go('login.loginForm');
					});
				}else{
					//Error Message (Standard)
		        $ionicPopup.prompt({
		          title: "Error:",
		          template: data.data.result
		        })
				}
      });
  }; /*End of User's Profile save button*/


  /*Start of User's Profile load() function*/
  $scope.load = function(){
	    /*** REMOVED THIS BLOCK COMMENT TO TEST IN MOBILE DEVICE ***

	    //initialised camera capture
	    if (!navigator.camera){
	      //error handling
	      return;
	    }
	    //pictureSource=navigator.camera.PictureSourceType.PHOTOLIBRARY;
	    pictureSource=navigator.camera.PictureSourceType.CAMERA;
	    destinationType=navigator.camera.DestinationType.FILE_URI;

      *** REMOVED THIS BLOCK COMMENT TO TEST IN MOBILE DEVICE ***/

	    $scope.pwd = {};
	    $scope.active = true;
		$scope.baseURL = window.localStorage.getItem('baseURL');
		var accessToken = window.localStorage.getItem('accessToken');
		$scope.account = {}; 			//Initialise account object container to be use

		$http.get($scope.baseURL + '/worker?accessToken=' + accessToken, {headers: {'Content-Type': 'application/json'}})
		.then(function(data, status, headers, config){
			//Success
			var result = data.data;
			$scope.account.username = data.data.loginID;
			$scope.account.password = data.data.password;
			$scope.account.firstname = data.data.firstname;
			$scope.account.lastname = data.data.lastname;
			$scope.account.email = data.data.email;
			$scope.account.contactNo = data.data.contactNo;
			$scope.account.userType = data.data.userType;
			$scope.account.countryCode = data.data.countryCode;
			$scope.account.skillID = data.data.skillID.toString(); //Convert integer to string format
			//$scope.account.ranking = data.data.ranking;
			//$scope.account.createdDate = data.data.createdDate;
			$scope.account.profilePict = data.data.profilePict;

			if($scope.account.profilePict == undefined){
				$scope.account.profilePict = "img/defaultProfilePict.jpg";
			}

		},function(data, status, headers, config){
			//401 invalid access token, back to login
			if(data.status===401){
				$ionicPopup.alert({
					title: "Session Timed out",
					template: "Going back to login screen"
				}).then(function(res){
					window.localStorage.removeItem("accessToken");
					$state.go('login.loginForm');
				});
			}else{
				//Error Message (Standard)
				$ionicPopup.prompt({
					title: "Error:",
					template: data.data.result
				})
			}
		});
	};/*End of User's Profile load() function*/



})
