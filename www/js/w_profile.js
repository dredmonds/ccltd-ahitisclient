/*****************************************************************
* Module Name: Worker's Profile
* Description: Worker's profile client controller
* Author: dredmonds
* Date Modified: 2015.01.19
* Related files: wkrprofile.html
*****************************************************************/
// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'

//Implement the controller profile.js
app.controller('wProfileCtrl', function($scope, $rootScope, $state, $http, $ionicHistory, $ionicPopup, md5) {

	  $scope.logout = function() {
		    var accessToken = window.localStorage.getItem('accessToken');
		    $http.get($scope.baseURL + '/logout?accessToken='+accessToken,{headers: {'Content-Type': 'application/json'}})
		    .then(function(data, status, headers, config){
					if($scope.worker){
						$scope.worker.onlineStatus = "Offline";
			      $scope.worker.toggleOnlineText = "Go Online";
			      $scope.worker.toggleButtonColor = "assertive";
					}
		      window.localStorage.setItem("onlineStatus", "Offline");
		      navigator.geolocation.clearWatch($rootScope.watchID);
		      delete $rootScope.watchID;

					window.localStorage.removeItem("accessToken");
		      $state.go('login.loginForm');
		    },function(data, status, headers, config){
					//401 invalid access token, back to login
					if(data.status===401){
						$ionicPopup.alert({
							title: "Session Timed out",
							template: "Going back to login screen"
						}).then(function(res){
							window.localStorage.removeItem("accessToken");
							$state.go('login.loginForm');
						});
					}else{
						//fail
			      $ionicPopup.alert({
			        title: "Error:",
			        template: data.data.result
			      });
					}
		    });
	  };//End of logout function


	  $scope.editProfile = function(){

		  var promptPopup = $ionicPopup.prompt({
			  title: 'VERIFY ACCOUNT',
			  template: 'Enter your password',
			  inputType: 'password',
			  inputPlaceholder: 'Type your password'
		  });

		  promptPopup.then(function(res){
			  if(res){
				  //console.log('Your input is ',res);
				  var accessToken = window.localStorage.getItem('accessToken');
				  var wkrpwdObj = {
						  accessToken: accessToken,
						  password: md5.createHash(res || '')
				  };

				  $http.post($scope.baseURL + '/wrkAuthPassword', wkrpwdObj, {headers: {'Content-Type': 'application/json'}})
				  .then(function(data){

					  if(data.status == 200){
						  promptPopup.close();
						  $state.go('worker.edit', {"loginID":$scope.loginID});
					  }

			    },function(data, status, headers, config){
						//401 invalid access token, back to login
						if(data.status===401){
							$ionicPopup.alert({
								title: "Session Timed out",
								template: "Going back to login screen"
							}).then(function(res){
								window.localStorage.removeItem("accessToken");
								$state.go('login.loginForm');
							});
						}else{
							//error
			          var result = data.data;
			          //403 login fail, wrong password or username
			          if(data.status === 403){
			            var promptAlert = $ionicPopup.alert({
						   			title: 'Error Message',
						   			template: 'Please enter a correct password'
						 			});
			          }else if(result === null){
			              var promptAlert = $ionicPopup.alert({
						     			title: 'Error Message',
						     			template: 'Data record not found'
						   			});
			          }
								promptAlert.then(function(res){
						    	promptPopup.close();
						    	promptAlert.close();
						    	$scope.editProfile();
					  		});
						}
				  });//End of http post /usrAuthPassword


			  }else if(res == ''){
				  //console.log('Please enter input');
				  var promptAlert = $ionicPopup.alert({
					  title: 'Error Message',
					  template: 'Please enter a password'
				  });
				  promptAlert.then(function(res){
					  promptPopup.close();
					  promptAlert.close();
					  $scope.editProfile();
				  });
			  }//End if res function
		  });//End promptPopup function

	  };//End of Edit Profile function




	  $scope.updateDepositCard = function(){

		  var promptPopup = $ionicPopup.prompt({
			  title: 'VERIFY ACCOUNT',
			  template: 'Enter your password',
			  inputType: 'password',
			  inputPlaceholder: 'Type your password'
		  });

		  promptPopup.then(function(res){
			  if(res){
				  //console.log('Your input is ',res);
				  var accessToken = window.localStorage.getItem('accessToken');
				  var wkrpwdObj = {
						  accessToken: accessToken,
						  password: md5.createHash(res || '')
				  };

				  $http.post($scope.baseURL + '/wrkAuthPassword', wkrpwdObj, {headers: {'Content-Type': 'application/json'}})
				  .then(function(data){

					  if(data.status == 200){
						  promptPopup.close();
						  $state.go('worker.depositcard', {"loginID":$scope.loginID});
					  }

			      },function(data, status, headers, config){
							//401 invalid access token, back to login
              if(data.status===401){
                $ionicPopup.alert({
                  title: "Session Timed out",
                  template: "Going back to login screen"
                }).then(function(res){
                  window.localStorage.removeItem("accessToken");
                  $state.go('login.loginForm');
                });
              }else{
								//error
				          var result = data.data;
				          //403 login fail, wrong password or username
				          if(data.status === 403){
				            var promptAlert = $ionicPopup.alert({
							   			title: 'Error Message',
							   			template: 'Please enter a correct password'
							 			});
				          }else if(result === null){
				              var promptAlert = $ionicPopup.alert({
							     			title: 'Error Message',
							     			template: 'Data record not found'
							   			});
				          }
									promptAlert.then(function(res){
								    promptPopup.close();
								    promptAlert.close();
								    $scope.updateDepositCard();
								  });
							}
				  });//End of http post /usrAuthPassword


			  }else if(res == ''){
				  //console.log('Please enter input');
				  var promptAlert = $ionicPopup.alert({
					  title: 'Error Message',
					  template: 'Please enter a password'
				  });
				  promptAlert.then(function(res){
					  promptPopup.close();
					  promptAlert.close();
					  $scope.updateDepositCard();
				  });
			  }//End if res function
		  });//End promptPopup function

	  };//End of updateDepositCard function


	$scope.load = function(){
		$scope.baseURL = window.localStorage.getItem('baseURL');
		var accessToken = window.localStorage.getItem('accessToken');
		$scope.account = {}; 			//Initialise account object container to be use

		$http.get($scope.baseURL + '/worker?accessToken=' + accessToken, {headers: {'Content-Type': 'application/json'}})
		.then(function(data, status, headers, config){
			//Success
			var result = data.data;
			$scope.loginID = data.data.loginID;
			$scope.account.username = data.data.loginID;
			$scope.account.password = data.data.password;
			$scope.account.firstname = data.data.firstname;
			$scope.account.lastname = data.data.lastname;
			$scope.account.email = data.data.email;
			$scope.account.contactNo = data.data.contactNo;
			$scope.account.userType = data.data.userType;
			$scope.account.countryCode = data.data.countryCode;
			$scope.account.ranking = data.data.ranking;
			$scope.account.duration = data.data.duration;
			$scope.account.amount = data.data.amount;
			$scope.account.amountPay = Math.floor(data.data.amount * 0.8 * 100)/100;
			$scope.account.createdDate = data.data.createdDate;
			$scope.account.profilePict = data.data.profilePict;

			if(!$scope.account.profilePict){
				$scope.account.profilePict = "img/defaultProfilePict.jpg";
			}

			if(data.data.skillID === 1){
				$scope.account.skillID = "Cleaning";
			} else if(data.data.skillID === 2){
				$scope.account.skillID = "Ironing";
			} else if(data.data.skillID === 3){
				$scope.account.skillID = "Gardening";
			} else if(data.data.skillID === 4){
				$scope.account.skillID = "Window and Gutter Cleaning";
			} else if(data.data.skillID === 5){
				$scope.account.skillID = "Car Washing";
			} else if(data.data.skillID === 6){
				$scope.account.skillID = "Animal Care - Dog walking, pet sitting";
			} else if(data.data.skillID === 7){
				$scope.account.skillID = "Personal Trainer";
			};


		},function(data, status, headers, config){
			//401 invalid access token, back to login
			if(data.status===401){
				$ionicPopup.alert({
					title: "Session Timed out",
					template: "Going back to login screen"
				}).then(function(res){
					window.localStorage.removeItem("accessToken");
					$state.go('login.loginForm');
				});
			}else{
				//Error Message (Standard)
				$ionicPopup.prompt({
					title: "Error:",
					template: data.data.result
				})
			}
		});
	};

})
