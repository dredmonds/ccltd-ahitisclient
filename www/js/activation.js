/*****************************************************************
* Module Name: Activation
* Description: Account activation controller
* Author: Brian Lai
* Date Modified: 2016.06.24
* Related files: activation.html
*****************************************************************/
app.controller('activationCtrl', function($scope, $rootScope, $state, $http, $ionicPopup, $ionicLoading, $window, $cordovaDevice) {
  //Prepare the baseURL object.
  $scope.baseURL = {};

  $scope.activate = function(account){
      //Obtain the baseURL object which stored the server address from app.js.
      $scope.baseURL = window.localStorage.getItem('baseURL');

      //prepare the activation object to send to server.
      var activationObj = {
        activationKey: account.activationKey
      };
      //login
      console.log("BaseURL" + $scope.baseURL);
      $rootScope.loading = $ionicLoading.show({
        template: 'Loading...',
      });
      $http.post($scope.baseURL + '/activation', activationObj, {headers: {'Content-Type': 'application/json'}})
      .then(function(data){
        $rootScope.loading.hide();
        //Success
        var result = data.data;
        var userType = data.data.userType;

        $ionicPopup.alert({
          title: "Activation",
          template: "Your AHitis account have been activated."
        }).then(function(res){
          if(userType === "User"){
            $state.go('user.main');
          }else{
            $state.go('worker.main');
          }
        });

      },function(data, status, headers, config){
        $rootScope.loading.hide();
        //error
        var result = data.data;
        var title = "";

        //403 login fail, wrong password or username
        if(data.status === 403){
          title = "Activation";
        }else{
          if(result === null){
            console.log(JSON.stringify({data: data, status: status, headers: headers, config: config}));
            $ionicPopup.alert({
              title: "Error",
              template: "Connection timeout. Please check your connection."
            });
            return;
          }
          title = "Error";
        }
        console.log(JSON.stringify({data: data, status: status, headers: headers, config: config}));
        $ionicPopup.alert({
          title: title,
          template: data.data.result
        });
      });
  }
})
