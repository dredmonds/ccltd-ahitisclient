/*****************************************************************
* Module Name: App start dispatcher
* Description: To check if user has access token and dispatch to appriate pages
* Author: Brian Lai
* Date Modified: 2016.01.04
* Related files: index.html
*****************************************************************/
app.controller('indexCtrl', function($state, $rootScope, $ionicPlatform, $cordovaDialogs, $cordovaMedia, $cordovaToast) {
  //notification variable
  $ionicPlatform.ready(function () {

    $rootScope.notifications = [];

if(ionic.Platform.isIOS() || ionic.Platform.isAndroid() || ionic.Platform.isIPad()){
    console.log("Preparing push notification");

                  var push = PushNotification.init({
                        "android":{
                          "senderID": "156670133332"
                        },
                        "ios": {"alert": "true", "badge": "true", "sound": "true"},
                        "windows": {}
                      });

                      push.on('registration', function(data){
                        console.log("Register success " + data.registrationId);

                        //$cordovaToast.showShortCenter('Registered for push notifications');
                        console.log("PushID: " + data.registrationId);
                        window.localStorage.setItem("pushID", data.registrationId);
                        $rootScope.regLoad();
                      });

                      push.on('notification', function(data){
                        console.log(JSON.stringify(data));
                        if (data.additionalData.foreground === true) {
                            // Play custom audio if a sound specified.
                            //if (data.sound) {
                            //  var mediaSrc = $cordovaMedia.newMedia(data.sound);
                            //  mediaSrc.promise.then($cordovaMedia.play(mediaSrc.media));
                            //}

                              console.log("payload: " + data.additionalData.payload);
                              if(!data.additionalData.payload){
                                console.log(JSON.stringify(data));
                              }
                              if (data.message) {
                                var msg = data.message.split(":");
                                if(msg.length===2){
                                  switch(msg[0]){
                                    case "Job Arrive":
                                      $cordovaDialogs.alert(msg[1], msg[0]);
                                      if($rootScope.loading){
                                        $rootScope.loading.hide();
                                      }
                                      if(data.additionalData.payload){
                                        $rootScope.jobID = data.additionalData.payload;
                                        delete $rootScope.job;
                                        $state.go("worker.jobDetail", {}, { reload: true });
                                      }
                                      break;
                                    case "Job Accepted":
                                      $cordovaDialogs.alert(msg[1], msg[0]);
                                      if($rootScope.loading){
                                        $rootScope.loading.hide();
                                      }
                                      if(data.additionalData.payload){
                                        $rootScope.jobID = data.additionalData.payload;
                                        delete $rootScope.job;
                                        $state.go("user.jobDetail", {}, { reload: true });
                                      }
                                      break;
                                    case "Job Rejected":
                                      $cordovaDialogs.alert(msg[1], msg[0]);
                                      if($rootScope.loading){
                                        $rootScope.loading.hide();
                                      }
                                      if(data.additionalData.payload){
                                        $rootScope.jobID = data.additionalData.payload;
                                        delete $rootScope.job;
                                        $state.go("user.main");
                                      }
                                    default:
                                      break;
                                  }
                                }else{
                                  $cordovaDialogs.alert(data.message, "Notification received");
                                }
                              }
                              else $cordovaDialogs.alert("Push Notification Received", "Push Notification Received");
                        }
                        else {
                          if (data.message) {
                            var msg = data.message.split(":");
                            if(msg.length===2){
                              switch(msg[0]){
                                case "Job Arrive":
                                  $cordovaDialogs.alert(msg[1], msg[0]);
                                  if($rootScope.loading){
                                    $rootScope.loading.hide();
                                  }
                                  if(data.additionalData.payload){
                                    $rootScope.jobID = data.additionalData.payload;
                                    delete $rootScope.job;
                                    $state.go("worker.jobDetail", {}, { reload: true });
                                  }
                                  break;
                                case "Job Accepted":
                                  $cordovaDialogs.alert(msg[1], msg[0]);
                                  if(data.additionalData.payload){
                                    $rootScope.jobID = data.additionalData.payload;
                                    delete $rootScope.job;
                                    $state.go("user.jobDetail", {}, { reload: true });
                                  }
                                  break;
                                case "Job Rejected":
                                  $cordovaDialogs.alert(msg[1], msg[0]);
                                  if(data.additionalData.payload){
                                    $rootScope.jobID = data.additionalData.payload;
                                    delete $rootScope.job;
                                    $state.go("user.jobDetail");
                                  }
                                default:
                                  break;
                              }
                            }else{
                              $cordovaDialogs.alert(data.message, "Notification received");
                            }
                          }
                          else $cordovaDialogs.alert("Push Notification Received", "Push Notification Received");
                        }

                        push.finish(function(){
                          console.log("Notification successfully received");
                        });
                      });

}

    //If this is not mobile device, force to login screen.
    console.log(ionic.Platform.device().model);
    if((!ionic.Platform.isIOS() && !ionic.Platform.isAndroid() && !ionic.Platform.isIPad()) || ionic.Platform.device().model === "x86_64"){
      $rootScope.regLoad();
    }else{
      $rootScope.TEST = "";
      //$rootScope.regLoad();
    }
  });


});
