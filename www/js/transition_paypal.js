app.controller('transitionPaypal_Ctrl', function($scope, $sce, $rootScope, $state, $stateParams, $http, $ionicPopup, $cordovaDevice) {
  //Prepare the baseURL object.
  $scope.baseURL = {};
  $scope.formPOST = {};

  $scope.load = function(){
    $scope.stringToPOST = {}
    $scope.stringToPOST.paymentToken = $stateParams.paymentToken;
    $scope.stringToPOST.orderID = $stateParams.orderID;
    $scope.stringToPOST.buttonID = $stateParams.buttonID;
    $scope.stringToPOST.hours = $stateParams.hours;
    $scope.stringToPOST.amount = $stateParams.amount.slice(0,$stateParams.amount.length - 2) + "." + $stateParams.amount.slice($stateParams.amount.length-2,$stateParams.amount.length);

    /*
    var paypalForm = document.getElementById('paypalForm');
    paypalForm.submit();
      //Obtain the baseURL object which stored the server address from app.js.
      $scope.baseURL = window.localStorage.getItem('baseURL');
      callPaypal($scope.baseURL, $stateParams.orderID, $stateParams.hours);
      */



	//$scope.formPOST = {};
	$scope.formPOST.orderID = $stateParams.orderID;
	$scope.formPOST.amount = $stateParams.amount.slice(0,$stateParams.amount.length - 2);
	$scope.formPOST.currency = $stateParams.currency;              //Need to replace with "$stateParams.currency" later

    /*Prepare to get Braintree payment token from server*/

	$scope.baseURL = window.localStorage.getItem('baseURL');
	var accessToken = window.localStorage.getItem('accessToken');

	$http.get($scope.baseURL + '/paymentToken?accessToken=' + accessToken, {headers: {'Content-Type': 'application/json'}})
	.then(function(data, status, headers, config){

		//Success
		var result = data.data;
		$scope.formPOST.paymentToken = data.data.paymentToken;
        $scope.formPOST.actionURL = $scope.baseURL + '/paymentTX?accessToken=' + accessToken;

        var clientToken = $scope.formPOST.paymentToken;

        /*Braintree Dropin Setup*/
        braintree.setup(clientToken, 'dropin', {
          container: 'payment-form',
          paypal: {
            singleUse: true,
            amount: $scope.formPOST.amount,
            currency: $scope.formPOST.currency,

            button: {
              type: 'checkout'
            }
          },

          onError: function (obj) {
            if (obj.type == 'VALIDATION') {
        	  // Validation errors contain an array of error field objects:
              $ionicPopup.alert({
                title: "Validation Error:",
                template: obj.details.invalidFields
              })
              $state.go('user.main');//Return back to user main page

        	} else if (obj.type == 'SERVER') {
              // If the customer's browser cannot connect to Braintree:
              $ionicPopup.alert({
                title: "Server" + obj.message,
                template: obj.details
              })
              $state.go('user.main');//Return back to user main page
        	}
          }

        });//End of braintee.setup


	},function(data, status, headers, config){
		//401 invalid access token, back to login
		if(data.status===401){
			$ionicPopup.alert({
				title: "Session Timed out",
				template: "Going back to login screen"
			}).then(function(res){
				window.localStorage.removeItem("accessToken");
				$state.go('login.loginForm');
			});
		}else{
			//Error Message (Standard)
			$ionicPopup.prompt({
				title: "Error:",
				template: data.data.result
			});
		}
	});




  }//End of load function



})
