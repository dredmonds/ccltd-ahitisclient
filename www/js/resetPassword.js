/*****************************************************************
* Module Name: Reset password
* Description: Reset Password controller
* Author: Brian Lai
* Date Modified: 2016.01.04
* Related files: resetPassword.html
*****************************************************************/
app.controller('resetPasswordCtrl', function($scope, $rootScope, $state, $http, $ionicPopup, $ionicLoading, $window, $cordovaDevice, md5) {
  //Prepare the baseURL object.
  $scope.baseURL = {};

  $scope.resetPassword = function(account){
      //Obtain the baseURL object which stored the server address from app.js.
      $scope.baseURL = window.localStorage.getItem('baseURL');

      //prepare the resetPassword object to send to server.
      var resetPasswordObj = {
        verificationCode: account.verificationCode,
        password: md5.createHash(account.password || '')
      };
      //login
      console.log("BaseURL" + $scope.baseURL);
      $rootScope.loading = $ionicLoading.show({
        template: 'Loading...',
      });
      $http.post($scope.baseURL + '/resetPassword', resetPasswordObj, {headers: {'Content-Type': 'application/json'}})
      .then(function(data){
        $rootScope.loading.hide();
        //Success
        var result = data.data;

        $ionicPopup.alert({
          title: "Reset password",
          template: "Your password has been reset."
        });

        $state.go('login.loginForm');
      },function(data, status, headers, config){
        $rootScope.loading.hide();
        //error
        var result = data.data;
        var title = "";
        //403 login fail, wrong password or username
        if(data.status === 403){
          title = "Reset Password Fail";
        }else{
          if(result === null){
            console.log(JSON.stringify({data: data, status: status, headers: headers, config: config}));
            $ionicPopup.alert({
              title: "Error",
              template: "Connection timeout. Please check your connection."
            });
            return;
          }
          title = "Error";
        }
        console.log(JSON.stringify({data: data, status: status, headers: headers, config: config}));
        $ionicPopup.alert({
          title: title,
          template: data.data.result
        });
      });
  }
})
