/*****************************************************************
* Module Name: Help
* Description: Help
* Author: HRoy
* Date Modified: 2016.06.30
* Related files: w_help.html
*****************************************************************/
app.controller('wHelpCtrl', function($scope, $rootScope, $state, $http, $stateParams, $ionicPopup, $ionicLoading, $window) {

	$scope.jtMoreInfo = function(){
		switch ($scope.selectedjob = document.getElementById("seljdex").value) {
			case "Cleaning":
				$scope.moreInformation = "The Ahitis worker can come to your home and perform household cleaning tasks.";
				break;
			case "Ironing":
				$scope.moreInformation = "The Ahitis worker can come to your home and help you with your Ironing.";
				break;
			case "Gardening":
				$scope.moreInformation = "The Ahitis worker can come to your home and make your Garden look nice.";
				break;
			case "Window and Gutter Cleaning":
				$scope.moreInformation = "The Ahitis worker can come to your home and clean Windows or Gutters.";
				break;
			case "Car Washing":
				$scope.moreInformation = "The Ahitis worker can be your own private Car Valet.";
				break;
			case "Animal Care - Dog walking, pet sitting":
				$scope.moreInformation = "The Ahitis worker can look after all of your pets needs.";
				break;
			case "Personal Trainer":
				$scope.moreInformation = "The Ahitis worker can be your own Personal Trainer in your own home.";
				break;
			default:
				$scope.moreInformation = "Select a Job Type for more Information.";
		}
	};

	$scope.validatejobTypes = function(){
		$scope.baseURL = window.localStorage.getItem('baseURL');
		var accessToken = window.localStorage.getItem('accessToken');
		$scope.skills = [];
		$http.get($scope.baseURL + '/skill/name?accessToken=' + accessToken, {headers: {'Content-Type': 'application/json'}})
		.then(function(data, status, headers, config){
			var result = data.data;
//
//			Test to see if dropdown has correct number of children
//
			var children = document.getElementById("seljdex").childElementCount;
			var inLen = data.data.skills.length;
			if(inLen != children) { // Needs to be loaded or reloaded
				var infanticide = document.getElementById("seljdex");
				var infanticide = document.getElementById("seljdex");
//
//        See if dropdown has been already populated
//
				if(children > 1) { // Clear dropdown
					for(var i = 0; i < children; i++){
						infanticide.removeChild(infanticide.childNodes[0]);
					}
				}
//
//				Load dropdown from database
//
				for(var idx in data.data.skills){
					var skillObj = {};
					skillObj.name = data.data.skills[idx].name;
					var element = document.getElementById("seljdex");
					var option = document.createElement("option");
					option.text = skillObj.name;
					element.add(option);
					$scope.skills.push(skillObj);
				}
			}
		},function(data, status, headers, config){
			//401 invalid access token, back to login
			if(data.status===401){
				$ionicPopup.alert({
					title: "Session Timed out",
					template: "Going back to login screen"
				}).then(function(res){
					window.localStorage.removeItem("accessToken");
					$state.go('login.loginForm');
				});
			}else{
				$ionicPopup.alert({title: "Error:", template: data.data.result})
			}
		});
		$state.go("worker.jtexample");
	};

	$scope.validatedisputes = function(){
		var selectedjob = $stateParams.jobID;
		var selectedreason = document.getElementById("seldd").value;
			if(selectedreason == "") {
				document.getElementById("seld").style.background = "red";
				document.getElementById("seld").style.color = "white";
				$scope.reasonlabel = 'Reason is Required';
			} else {
			$scope.reasonlabel = 'Reason';
			document.getElementById("seld").style.background = "white";
			document.getElementById("seld").style.color = "black";
			$scope.baseURL = window.localStorage.getItem('baseURL');
			var accessToken = window.localStorage.getItem('accessToken');
			var disputeObj = {
				accessToken: accessToken,
				jobID : selectedjob,
				reason : selectedreason
			};
			$rootScope.loading = $ionicLoading.show({
	      template: 'Loading...',
	    });
			$http.post($scope.baseURL + '/dispute', disputeObj, {headers: {'Content-Type': 'application/json'}})
			.then(function(data, status, headers, config){
				$rootScope.loading.hide();
				//success
				var result = data.data;
				$ionicPopup.alert({
					title: "Dispute",
					template: result.result
				}).then(function(res){
					$state.go('worker.help');
				});
			},function(data, status, headers, config){
				$rootScope.loading.hide();
				//401 invalid access token, back to login
				if(data.status===401){
					$ionicPopup.alert({
						title: "Session Timed out",
						template: "Going back to login screen"
					}).then(function(res){
						window.localStorage.removeItem("accessToken");
						$state.go('login.loginForm');
					});
				}else{
					//Error
					$ionicPopup.alert({
						title: "Dispute Error:",
						template: data.data.result
					});
				}
			});
		};
	};

	$scope.validateerror = function(error){
		var errorDesc = error.description;
		$scope.baseURL = window.localStorage.getItem('baseURL');
		var accessToken = window.localStorage.getItem('accessToken');
		var errorRepObj = {
			accessToken: accessToken,
			errorRep : errorDesc
		};
		$rootScope.loading = $ionicLoading.show({
			template: 'Loading...',
		});

		$http.post($scope.baseURL + '/errorrep', errorRepObj, {headers: {'Content-Type': 'application/json'}})
		.then(function(data, status, headers, config){
			$rootScope.loading.hide();
			//success
			var result = data.data;
			$ionicPopup.alert({
				title: "Error report",
				template: result.result
			}).then(function(res){
				$state.go('worker.help');
			});
		},function(data, status, headers, config){
			$rootScope.loading.hide();
			//401 invalid access token, back to login
			if(data.status===401){
				$ionicPopup.alert({
					title: "Session Timed out",
					template: "Going back to login screen"
				}).then(function(res){
					window.localStorage.removeItem("accessToken");
					$state.go('login.loginForm');
				});
			}else{
				//Error
				$ionicPopup.alert({
					title: "Error Report",
					template: data.data.result
				});
			}
		});
};

	$scope.load = function(){
		$scope.topictitle = 'Dispute a Job';
		$scope.topicreason = 'Reason for Dispute';
		$scope.reasonlabel = 'Select Reason';
		$scope.topicfootnote = 'Disputes via Ahitis should only be made when there is confusion or disagreement about hours worked by the helper.';

		$scope.jobtypes = function() {
			$scope.topictitle = 'Job Types';
			$scope.topicreason = '';
			$scope.reasonlabel = '';
			$scope.topicfootnote = '';
			$state.go("worker.help.jobtypes");
		};

		$scope.creatingajob = function() {
			$scope.topictitle = 'Creating a Job';
			$scope.topicreason = '';
			$scope.reasonlabel = '';
			$scope.topicfootnote = '';
			$state.go("worker.help.creatingajob");
		};

		$scope.qanda = function() {
			$scope.topictitle = 'Error reporting';
			$scope.topicreason = '';
			$scope.reasonlabel = '';
			$scope.topicfootnote = '';
			$state.go("worker.help.qanda");
		};

		$scope.disputes = function() {
			$scope.topictitle = 'Dispute a Job';
			$scope.topicreason = 'Reason for Dispute';
			$scope.reasonlabel = 'Select Reason';
			$scope.topicfootnote = 'Disputes via Ahitis should only be made when there is confusion or disagreement about hours worked by the helper.';
			$state.go("worker.help.jobList", {"scope":"dispute"});
		};
	};
})
